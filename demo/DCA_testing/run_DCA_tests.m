% this script runs several tests on the DCA, usefull to check whether
% everything is still working properly
close all
clear all
clc


cd V:\Toolbox\DistortionContributionAnalysis\Demo\DCA_testing

unbal2bal = false;
if unbal2bal
    ADSres = ADSimportSimData(fullfile(pwd,'ADS simresults','DCAtest_Linear_Analysis_unbal2bal.mat'));
else
    ADSres = ADSimportSimData(fullfile(pwd,'ADS simresults','DCAtest_Linear_Analysis.mat'));
end
ADSres = ADSres.sim1_S;

ADS.SINGLE.G = ADSres.S(1:2,1:2,:);
ADS.DIFF.G = ADSres.S(3:6,3:6,:);
ADS.MIXED.G = ADSres.S(7:9,7:9,:);
ADS.freq = ADSres.freq;

MSdef = MScreate(min(ADSres.freq),max(ADSres.freq));
MSdef.MSnode =  {'MSnode','0'};

BLAs = DCA('DCAtest_multiStage.net','MSdef',MSdef,'unbal2bal',unbal2bal,...
    'BLAtechnique','ZIPPER','loadOldResults',false,'save',false,'cleanup',true,'testPhase','BLA');

% [BLAs,spec,waves,specraw,wavesraw] = Zipper_Analysis('DCAtest_multiStage.net',MSdef,...
%             'numberOfRealisations',3,'TicklerAmplitudes',1e-6,...
%             'unbal2bal',unbal2bal,'StagesToEstimate',{'DIFF'},'OneByOne',true,...
%             'TicklerDistance',MSdef.f0/2,'useLargeMS',false);        
        
        
%%
fun=@db;
try
    h=figure(1);
    set(h,'name','SINGLE');
    plot_mimo(BLAs.freq,fun(BLAs.SINGLE.G),'r-');
    plot_mimo(ADS.freq,fun(ADS.SINGLE.G),'b+');
    try plot_mimo(fun(ADS.SINGLE.G-BLAs.SINGLE.G),'k-');end
    title('blue: ADS              red: DCA')
catch
    close(1)
end
try
    h=figure(2);
    set(h,'name','DIFF');
    plot_mimo(BLAs.freq,fun(BLAs.DIFF.G),'r-');
    plot_mimo(ADS.freq,fun(ADS.DIFF.G),'b+');
    try plot_mimo(fun(ADS.DIFF.G-BLAs.DIFF.G),'k');end
    title('blue: ADS              red: DCA')
catch
    close(2)
end
try
    h=figure(3);
    set(h,'name','MIXED');
    plot_mimo(BLAs.freq,fun(BLAs.MIXED.G),'r-');
    plot_mimo(ADS.freq,fun(ADS.MIXED.G),'b+');
    try plot_mimo(fun(ADS.MIXED.G-BLAs.MIXED.G),'k');end
    title('blue: ADS              red: DCA')
catch
    close(3)
end

% figure(10)
% toplot = 'W_DIFF_p1m_B';
% for ii=1:3:size(wavesraw.(toplot),1)-3;
%     hold all
%     plot(wavesraw.freq,squeeze(db(wavesraw.(toplot)(ii:ii+3,end,:))),'+')
% end


%% run the test where the linear compensation is performed

% because there are no noise sources in the circuit, all distortion sources
% should be zero in the circuit
RES = DCA('DCAtest_multiStage.net','MSdef',MSdef,'BLAtechnique','Sparam','unbal2bal',unbal2bal,...
    'loadOldResults',false,'save',false,'cleanup',true,'display',true);% ,'testPhase','Comp'


%% perform a test where a multisine source is added to each sub-circuit
% the added multisines are uncorrelated, so the distortion contributions
% should resemble the actual noise analysis results
clear all
close all
clc
cd V:\Toolbox\DistortionContributionAnalysis\demo\DCA_testing\

% add all the multisines to the different nodes
MSnodes = {'MIXED_MS_c','MIXED_MS_d','DIFF_MS_c','DIFF_MS_d','SINGLE_MS'};%
% first do the main multisine
fmin = 0.1e9;fmax = 10e9;
MSdef = MScreate(fmin,fmax,'Rout',0,'grid','random-odd');
MSdef.MSnode = {'MSnode','0'};
% now add the noise multisines
for nn=1:length(MSnodes)
	tempMS=MScreate(fmin,fmax,'Rout',Inf,'rms',1e-6,'grid','random');
    tempMS.MSnode = {MSnodes{nn},'0'};
    MSdef(nn+1) = tempMS;
end
% do simulations with the noise sources active
try
    load(fullfile(pwd,'MultisineSimulations.mat'));
catch
    [waves,spec] = ADSsimulateMSwaves('DCAtest_multiStage.net',MSdef,...
        'simulator','HB','numberOfRealisations',100,'oversample',2);
    save('MultisineSimulations.mat','waves','spec');
end

% now do the DCA with these fake noise sources
RES = DCA('DCAtest_multiStage.net','MSdef',MSdef(1),'BLAtechnique','Sparam','unbal2bal',true,...
    'uncorr',true,'loadOldResults',true,'saveSimResults',false,'cleanup',true,'Display',true);

% plot the contributions of the different sources to the output node
plotContributions(RES.CONTs,1e9,RES.MSdef.info);

figure(1008)
clf
surf(db(RES.Cd(:,:,50)))
hold on
surf(db(RES.Cd(:,:,49)))
