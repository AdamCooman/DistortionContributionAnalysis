clc
clear all
close all


MSdef = MScreate(20e3,1000e3,'grid','odd','amp',0.0005);
MSdef.MSnode = {'Vind','0'};

% This demo shows the capability of the DCA when a fully differential circuit
% is provided to the method. 

RES = DCA('biquad.net',MSdef,'BLAtechnique','SPARAM','numberOfRealisations',50);

%%

plotContributions(RES.CONTs,'freqToplot',[]);

%% plot the output spectrum


% calculate the distortion in the output wave
[G,Y,~,exBins] = calculateSISO_BLA(RES.waves,'input','ref','reference','ref','output','W_TOTAL_p2d_B');
nexbins = setdiff(2:2:length(RES.waves.freq),exBins);

Mtoplot = 1:6;

figure(501)
clf
hold on
plot(RES.waves.freq(exBins) /1e6,squeeze(db(RES.waves.W_TOTAL_p2d_B(Mtoplot,end,exBins ))),'k.','markersize',1);
plot(RES.waves.freq(1:2:end)/1e6,squeeze(db(RES.waves.W_TOTAL_p2d_B(Mtoplot,end,1:2:end))),'b.','markersize',1);
plot(RES.waves.freq(nexbins)/1e6,squeeze(db(RES.waves.W_TOTAL_p2d_B(Mtoplot,end,nexbins))),'r.','markersize',1);
plot(RES.waves.freq(2:2:end)/1e6,db(Y.disto(2:2:end)),'r-','Linewidth',2);
plot(RES.waves.freq(3:2:end)/1e6,db(Y.disto(3:2:end)),'b-','Linewidth',2);
plot(RES.BLAs.freq(1:2:end)/1e6,db(squeeze(sum(sum(RES.CONTs.TOTAL_p2d(:,:,1:2:end),1),2)))/2,'r+');
plot(RES.BLAs.freq(2:2:end)/1e6,db(squeeze(sum(sum(RES.CONTs.TOTAL_p2d(:,:,2:2:end),1),2)))/2,'b+');



