function [BLAs,spec,waves,specraw,wavesraw,MSdefs] = Zipper_Analysis(Netlist,MSdef,varargin)
% Zipper_Analysis Determines the BLA of all sub-circuits using zippered tickler multisines
% 
%      [BLAs,spec,waves,specraw,wavesraw] = Zipper_Analysis(Netlist,MSdef);
%      [BLAs,spec,waves,specraw,wavesraw] = Zipper_Analysis(Netlist,MSdef,'ParamName',paramValue,...);
% 
% 
% Required inputs:
% - Netlist      check: @(x) iscellstr(x)||ischar(x)
%      ADS netlist that contains the circuit
% - MSdef      check: @isstruct
%      definition of the large multisine in the circuit
% 
% Parameter/Value pairs:
% - 'numberOfRealisations'     default: 7  check: @isnatural
%      number of realisations used in the estimation of the BLA
% - 'StagesToEstimate'     default: {}  check: @iscellstr
%      You can use this to specify the stages that have to be estimated
%      by the  method. If you leave it empty, all stages and the TOTAL
%      stage is estimated
% - 'useLargeMS'     default: true  check: @islogical
%      if this is set to true, then the large multisine is incorporated
%      in the  BLA estimation. If not, only ticklers are used.
% - 'CF'     default: Inf  check: @isscalar
%      maximum crest factor of the multisines
% - 'unbal2bal'     default: true  check: @islogical
%      set this to true if you want conversion from unbalanced to mixed-mode
%       S-parameters
% - 'plot'     default: false  check: @islogical
%      set this to true if you want so see several plots with the intermediate
%       steps in the algorithm
% - 'OneByOne'     default: false  check: @islogical
%      if this is set to true, the ticklers are applied one by one
% - 'TicklerDistance'     default: []  check: @isscalar
%      use this to control the distance between the main multisine
%      grid and the  tickler grid
% 
% Outputs:
% - BLAs      Type: struct
%      contains the estimate of the BLA of the different stages in
%      the circuit. The struct has fields with the same name as the
%      stage itself. Each of the fields contains a BLA estimate G and
%      its covariance and variance matrices CvecG and varG.
% - spec      Type: struct
%      contains the simulation results without the tickler tones. The
%      spec struct contains the currents and voltages in the circuit
% - waves      Type: struct
%      contains the simulation results without the tickler tones. The
%      waves struct contains the waves in the circuit
% - specraw      Type: struct
%      contains the same as the spec output, but with the tickler bins
%      still added.
% - wavesraw      Type: struct
%      contains the same as the waves output, but with the tickler
%      bins still added.
% 
% Adam Cooman, ELEC, VUB
% 
% 
% This documentation was generated with the generateFunctionHelp function at 28-Aug-2015


p = inputParser();
% ADS netlist that contains the circuit
p.addRequired('Netlist',@(x) iscellstr(x)||ischar(x));
% definition of the large multisine in the circuit
p.addRequired('MSdef',@isstruct);
% number of realisations used in the estimation of the BLA
p.addParameter('numberOfRealisations',7,@isnatural);
% You can use this to specify the stages that have to be estimated by the
% method. If you leave it empty, all stages and the TOTAL stage is estimated
p.addParameter('StagesToEstimate',{},@iscellstr);
% if this is set to true, then the large multisine is incorporated in the
% BLA estimation. If not, only ticklers are used.
p.addParameter('useLargeMS',true,@islogical);
% maximum crest factor of the multisines
p.addParameter('CF',Inf,@isscalar);
% set this to true if you want conversion from unbalanced to mixed-mode
% S-parameters
p.addParameter('unbal2bal',true,@islogical);
% set this to true if you want so see several plots with the intermediate
% steps in the algorithm
p.addParameter('plot',false,@islogical);
% if this is set to true, the ticklers are applied one by one
p.addParameter('OneByOne',false,@islogical);
% use this to control the distance between the main multisine grid and the
% tickler grid
p.addParameter('TicklerDistance',[],@isscalar);
% characteristic impedance of the waves
p.addParameter('Z0',50,@isscalar);
p.KeepUnmatched = true;
p.StructExpand = true;
p.parse(Netlist,MSdef,varargin{:});
args = p.Results();
SimInfo = p.Unmatched;
clear Netlist MSdef varargin p

% fix the MSnode to be a cell for sure
if ~iscell(args.MSdef.MSnode)
    args.MSdef.MSnode = {args.MSdef.MSnode '0'};
end

% if the TicklerDistance is not specified, set it to 1/100 of the multisine base frequency
if isempty(args.TicklerDistance)
    args.TicklerDistance = args.MSdef.f0/100;
end

% if needed, read the provided file into a cell array of strings
if ischar(args.Netlist) 
    args.Netlist = readTextFile(args.Netlist);
end 

% make sure the voltage at each current source is measured
args.Netlist = prepareWaveSimulation(args.Netlist);

% analyse the netlist to obtain the amount of ports for each stage in the netlist
[info.Extern,info.Stages,info.StageNames,...
    info.externNodes,info.StageNodes,...
    info.diffExPorts,info.diffStPorts] = analyseNetlist(args.Netlist);

% if the user provides the stages that have to be estimated, then remove
% all the others from the info list
info = selectStages(info,args.StagesToEstimate);
if ~isempty(args.StagesToEstimate)
    estimateTOTAL = any(strcmpi(args.StagesToEstimate,'TOTAL'));
else
    estimateTOTAL = true;
end

% sort the stages by amount of ports
info = sortStages(info);

% collect the inputs and outputs names in a cell array. First TOTAL
names = {};
inputs = {};
outputs = {};
if estimateTOTAL
    names{end+1} = 'TOTAL';
    inputs{end+1}={};
    outputs{end+1}={};
    for pp=1:(info.Extern-length(info.diffExPorts))
        if ~any(info.diffExPorts==pp)
            inputs{1}{end+1}  = sprintf('W_TOTAL_p%d_A',pp);
            outputs{1}{end+1} = sprintf('W_TOTAL_p%d_B',pp);
        else
%             if ~args.unbal2bal
                inputs{1}{end+1}  = sprintf('W_TOTAL_p%dp_A',pp);
                inputs{1}{end+1}  = sprintf('W_TOTAL_p%dm_A',pp);
                outputs{1}{end+1} = sprintf('W_TOTAL_p%dp_B',pp);
                outputs{1}{end+1} = sprintf('W_TOTAL_p%dm_B',pp);
%             else
%                 inputs{1}{end+1}  = sprintf('W_TOTAL_p%dd_A',pp);
%                 inputs{1}{end+1}  = sprintf('W_TOTAL_p%dc_A',pp);
%                 outputs{1}{end+1} = sprintf('W_TOTAL_p%dd_B',pp);
%                 outputs{1}{end+1} = sprintf('W_TOTAL_p%dc_B',pp);
%             end
        end
    end
end

% collect the inputs and outputs names in a cell array. Now the other stages
for ss=1:length(info.Stages)
    names{end+1}=info.StageNames{ss};%#ok
    inputs{end+1} = {};%#ok
    outputs{end+1}= {};%#ok
    for pp=1:info.Stages(ss)-length(info.diffStPorts{ss})
        if ~any(info.diffStPorts{ss}==pp)
            inputs{end}{end+1}  = sprintf('W_%s_p%d_A',info.StageNames{ss},pp);
            outputs{end}{end+1} = sprintf('W_%s_p%d_B',info.StageNames{ss},pp);
        else
%             if ~args.unbal2bal
                inputs{end}{end+1}  = sprintf('W_%s_p%dp_A',info.StageNames{ss},pp);
                inputs{end}{end+1}  = sprintf('W_%s_p%dm_A',info.StageNames{ss},pp);
                outputs{end}{end+1} = sprintf('W_%s_p%dp_B',info.StageNames{ss},pp);
                outputs{end}{end+1} = sprintf('W_%s_p%dm_B',info.StageNames{ss},pp);
%             else
%                 inputs{end}{end+1}  = sprintf('W_%s_p%dd_A',info.StageNames{ss},pp);
%                 inputs{end}{end+1}  = sprintf('W_%s_p%dc_A',info.StageNames{ss},pp);
%                 outputs{end}{end+1} = sprintf('W_%s_p%dd_B',info.StageNames{ss},pp);
%                 outputs{end}{end+1} = sprintf('W_%s_p%dc_B',info.StageNames{ss},pp);
%             end
        end
    end
end

% build the list of all the nodes that should be excited. I put the nodes
% of the highest port numbers first, because they should be furthest away
% from the large source. The nodes of the total circuit are also put first,
% because that has to be estimated with ticklers outside its network.
nodeList = {};
if estimateTOTAL
    for pp=info.Extern:-1:1
        nodeList{end+1}=info.externNodes{pp}{1};
    end
end
for ss=1:length(info.Stages)
    for pp=info.Stages(ss):-1:1
        nodeList{end+1}=info.StageNodes{ss}{pp}{1};
    end
end

% if the output impedance of the multisine source is zero, we cannot
% connect a current source to that node. So if it's in the nodelist, remove
% that node from the nodelist
if args.MSdef.Rout==0
    nodeList = nodeList(~strcmp(args.MSdef.MSnode{1},nodeList));
end

% the amount of ticklers needed is equal to the maximum amount of ports of
% one of the sub-circuits minus one, because the multisine does stuff as well
if args.useLargeMS
    T = max([info.Extern info.Stages])-1;
else
    T = max([info.Extern info.Stages]);
end
ticklernodes = nodeList(1:T);

% the choice of extra nodes can probably be improved. Now, just the first
% nodes are chosen, but it is not guaranteed that this is the best option

%% run the tickler simulation
if ~args.OneByOne
    
    [specraw,MSdefs] = runZipperSimulation(args.Netlist,args.MSdef,'numberOfRealisations',args.numberOfRealisations,...
        'TicklerNodes',ticklernodes,'measure',true,'TicklerDistance',args.TicklerDistance,SimInfo);
    % calculate the waves in the simulation
    wavesraw = makeallwaves(specraw,50,'pseudo',true);

    % the reference signal in the BLA estimation is given by the tickler currents
    reference{1} = args.MSdef.MSnode{1};
    for pp=1:length(ticklernodes)
        reference{pp+1} = sprintf('I_tickle%d',pp);
    end
    
    % also add the reference currents to the waves struct
    for pp=1:length(reference)
        wavesraw.(reference{pp}) = specraw.(reference{pp});
    end
else
    specraw = cell(length(ticklernodes),1);
    wavesraw = cell(length(ticklernodes),1);
    reference = cell(length(ticklernodes),1);
    for tt=1:length(ticklernodes)
        % run a simulation with a tickler one at a time
        [specraw{tt},MSdefs{tt}] = runZipperSimulation(args.Netlist,args.MSdef,'numberOfRealisations',args.numberOfRealisations,...
            'TicklerNodes',ticklernodes{tt},'measure',true,'TicklerDistance',args.TicklerDistance,...
            SimInfo);
        % calculate the waves in the simulation
        wavesraw{tt} = makeallwaves(specraw{tt},50,'pseudo',true);
        
        % there are two reference signals  in this case: 
        % - the main multisine source
        % - and the single tickler current
        reference{tt}{1} = args.MSdef.MSnode{1};
        reference{tt}{2} = 'I_tickle1';
        
        % add the reference signals to the waves struct
        for pp=1:length(reference{tt})
            wavesraw{tt}.(reference{tt}{pp}) = specraw{tt}.(reference{tt}{pp});
        end
    end
end



%% now identify all the BLAs in the circuit
for ss=1:length(names)
    % extractMIMO_BLA_zippered estimates the SISO BLA from each reference
    % to the inputs and outputs of the system, then interpolates the SISO
    % BLAs and performs a MIMO estimation of the system
    [BLAs.(names{ss})] = extractMIMO_BLA_zippered(wavesraw,...
        'input',inputs{ss},...
        'output',outputs{ss},...
        'reference',reference,'plot',args.plot);
    
    % clean up the frequency axis in the BLA estimate. The
    % extractMIMO_BLA_zippered returns the BLA on the bins of the multisine
    % and on the bins of the tickler signals. The BLA should only be known
    % on the grid of the main multisine. (small extrapolation is possible here) 
    BLAs.(names{ss}) = cleanBLAgrid(BLAs.(names{ss}),args.MSdef);
    
    % the frequency axis is the same for all BLAs and should be saved at the top level of the struct
    if ss==1
        BLAs.freq = BLAs.(names{ss}).freq;
    end
    BLAs.(names{ss})=rmfield(BLAs.(names{ss}),'freq');
end

%% clean up the results a little

% remove the reference signal from the waves struct
if args.OneByOne
    for ss=1:length(wavesraw)
        for pp=1:length(reference{ss})
            wavesraw{ss} = rmfield(wavesraw{ss},reference{ss}{pp});
        end
    end
    wavesraw = [wavesraw{:}];
    specraw = [specraw{:}];
else
    for pp=1:length(reference)
        wavesraw = rmfield(wavesraw,reference{pp});
    end
end

wavestemp = wavesraw;
wavefields = fieldnames(wavesraw);
spectemp = specraw;
specfields = fieldnames(specraw);
wavesraw = wavestemp(1);
specraw = spectemp(1);
for ss=2:length(wavestemp)
    for ff=1:length(wavefields)
        if ~strcmp(wavefields{ff},'freq')
            wavesraw.(wavefields{ff})(end+1:end+args.numberOfRealisations,:,:) = wavestemp(ss).(wavefields{ff});
        end
    end
    for ff=1:length(specfields)
        if ~strcmp(specfields{ff},'freq')
            specraw.(specfields{ff})(end+1:end+args.numberOfRealisations,:,:) = spectemp(ss).(specfields{ff});
        end
    end
end

% remove the tickler bins from the spec and waves structs
[spec,waves]=cleanupSimresults(specraw,wavesraw,args.MSdef.f0,args.TicklerDistance);

%% Conversion to mixed-mode S-parameters when needed

if args.unbal2bal
    % now apply the unbalanced to balanced transform to all stages
    for ss=1:length(info.Stages)
        [BLAs.(info.StageNames{ss}).G,BLAs.(info.StageNames{ss}).CvecG] = ...
            unbalanced_to_balanced(BLAs.(info.StageNames{ss}).G,...
            info.diffStPorts{ss},args.Z0,...
            BLAs.(info.StageNames{ss}).CvecG);
        BLAs.(info.StageNames{ss}).varG = reshape(diag_FRM(BLAs.(info.StageNames{ss}).CvecG),size(BLAs.(info.StageNames{ss}).G));
    end
    % the S-parameters of the TOTAL circuit should be converted to differential as well
    if isfield(BLAs,'TOTAL')
        [BLAs.TOTAL.G,BLAs.TOTAL.CvecG] = ...
            unbalanced_to_balanced(BLAs.TOTAL.G,...
            info.diffExPorts,args.Z0,...
            BLAs.(info.StageNames{ss}).CvecG);
        BLAs.TOTAL.varG = reshape(diag_FRM(BLAs.TOTAL.CvecG),size(BLAs.TOTAL.G));
    end
end

%% save the port type in the struct as well, to make indicate the differential and common-mode ports
for ss=1:length(info.StageNames)
    BLAs.(info.StageNames{ss}).porttype = savePortType(info.Stages(ss),info.diffStPorts{ss}, args.unbal2bal );
end
if isfield(BLAs,'TOTAL')
    BLAs.TOTAL.porttype = savePortType( info.Extern , info.diffExPorts , args.unbal2bal );
end


end

%% sortStages function to sort the stages according to ports
function info = sortStages(info)
% sorts the stages on the amount of ports
[info.Stages,order] = sort(info.Stages,'descend');
info.StageNames = info.StageNames(order);
info.StageNodes = info.StageNodes(order);
info.diffStPorts= info.diffStPorts(order);
end

%% cleanupSimresults, to remove the tickler bins
function [spec,waves]=cleanupSimresults(specraw,wavesraw,f0,d)
% this function removes all frequency bins from a simulation result that
% are not integer multiples of f0;

% find the bins by looking at the frequency axis and finding the correct bins
bins = find(abs(specraw.freq - round(specraw.freq/f0)*f0)<d/2);
spec.freq = specraw.freq(bins);
waves.freq = wavesraw.freq(bins);

% clean up the spec struct
fields = fieldnames(specraw);
for ii=1:length(fields)
    if ~strcmp(fields{ii},'freq')
        spec.(fields{ii}) = specraw.(fields{ii})(:,:,bins);
    end
end
% clean up the waves struct
fields = fieldnames(wavesraw);
for ii=1:length(fields)
    if ~strcmp(fields{ii},'freq')
        waves.(fields{ii}) = wavesraw.(fields{ii})(:,:,bins);
    end
end
end

%% cleanBLAgrid: to remove some frequencies from the BLA estimation
function BLA = cleanBLAgrid(BLA,MSdef)
% this function interpolates and extrapolates the BLA to the grid of the MSdef
freqnew = MSdef.f0*(min(MSdef.grid):max(MSdef.grid));
% now just interpolate all the things. There will be some extrapolation at
% the edge, but if the ticklers lie close enough to the actual multisine
% grid, I don't care about that
BLA.G = interpolate_MIMO(BLA.freq,BLA.G,freqnew);
BLA.CvecG = interpolate_MIMO(BLA.freq,BLA.CvecG,freqnew);
BLA.varG = interpolate_MIMO(BLA.freq,BLA.varG,freqnew);
BLA.freq = freqnew;
end

%% selectStages: to allow estimating specified stages only
function info = selectStages(info,StagesToEstimate)
% this function allows to select some of the stages only, to avoid having
% to estimate them all. This is done by removing several of the stages from
% the info struct. That way, it appears the other stages don't exist
if ~isempty(StagesToEstimate)
    % check whether the specified stages are actual stages in the circuit.
    % and save their index in the stageList
    inds = [];
    for ss=1:length(StagesToEstimate)
        if ~strcmpi(StagesToEstimate{ss},'TOTAL')
            ind = find(strcmp(StagesToEstimate{ss},info.StageNames));
            if isempty(ind)
                error(['Stage "' StagesToEstimate{ss} '" has not been found in the circuit']);
            else
                inds(end+1) = ind;
            end
        end
    end
    % now remove all the other stages from the lists
    info.Stages = info.Stages(inds);
    info.StageNames = info.StageNames(inds);
    info.StageNodes = info.StageNodes(inds);
    info.diffStPorts = info.diffStPorts(inds);
end
end


% @generateFunctionHelp

% @author Adam Cooman
% @institution ELEC, VUB

% @tagline Determines the BLA of all sub-circuits using zippered tickler multisines

% @output1 contains the estimate of the BLA of the different stages in the
% @output1 circuit. The struct has fields with the same name as the stage itself.
% @output1 Each of the fields contains a BLA estimate G and its covariance and
% @output1 variance matrices CvecG and varG.
% @outputType1 struct

% @output2 contains the simulation results without the tickler tones. The
% @output2 spec struct contains the currents and voltages in the circuit
% @outputType2 struct

% @output3 contains the simulation results without the tickler tones. The
% @output3 waves struct contains the waves in the circuit
% @outputType3 struct

% @output4 contains the same as the spec output, but with the tickler bins
% @output4 still added.
% @outputType4 struct

% @output5 contains the same as the waves output, but with the tickler bins
% @output5 still added.
% @outputType5 struct
