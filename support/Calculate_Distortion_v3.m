function [BLAs,Cd,subcircuits,freq] = Calculate_Distortion_v3(spec,BLAs,type,uncorr)
% Calculates the distortion covariance matrix of an interconnected system
%
% BLAs = Calculate_Distortion_v2(spec , BLAs)
% [BLAs,Cd,subcircuits] = Calculate_Distortion_v3(spec,BLAs,type,uncorr)
%
% spec      struct with the MxPxF matrices that contain the measured signals
% BLAs      struct that contains the NxNxF S matrix. each NxN matrix is block diagonal
% type      type of BLA used. This can be 'S', 'Y', or 'Z'
% uncorr    boolean to indicate whether the distortion of different stages
%           can be considered uncorrelated
%
% BLAs      same struct as before, but the Ys field has been added to all BLAs
% Cd        NxNxF covariance matrix of all the distortion introduced by the stages.
% subcircuits cell array of strings that contains the names of the
%             subcircuits in order
%
% Adam Cooman, ELEC VUB


switch nargin
    case {0,1}
        error('at least 2 inputs are required')
    case 2
        % the default type for the BLA is S-parameters, because waves are awesome
        type = 'S';
        % and the waves are assumed to be correlated
        uncorr = false;
    case 3
        uncorr = false;
    case 4
        % nothing
    otherwise
        error('maximum 4 inputs');
end

% extract the subcircuits from the BLAs struct. If the package is present,
% use the portNames field there to get the correct order. If not, just get
% the fields out of the struct.
if isfield(BLAs,'PACKAGE')
    % look into the portNames of the package to find which stage is connected where
    subcircuits = regexp(BLAs.PACKAGE.portNames,'_p[0-9]+p?m?c?d?$','split');
    subcircuits = unique(cellfun(@(x) x{1},subcircuits,'UniformOutput',false),'stable');
else
    % look at the fieldnames in the BLAs struct, they will tell us which sub-circuits to consider
    subcircuits = fieldnames(BLAs);
    % ignore PACKAGE, LOAD and freq in the following
    % I don't ignore TOTAL, this way, its distortion will be calculated later on as well
    subcircuits = subcircuits(cellfun(@(x) ~any(strcmp(x,{'PACKAGE','LOAD','freq'})),subcircuits));
end

% if the porttype is not filled in into the BLAs, just assume it is a normal port
for ss=1:length(subcircuits)
    if ~isfield(BLAs.(subcircuits{ss}),'porttype')
        P = size(BLAs.(subcircuits{ss}).G,1);
        BLAs.(subcircuits{ss}).porttype = strsplit(num2str(1:P),' ');
    end
end

% do the frequency axes match? if not, interpolate the BLA to the frequency
% axis of the measured signal
inbandbins = (spec.freq>=(min(BLAs.freq)-1e-4)) & (spec.freq<=(max(BLAs.freq)+1e-4)); 
% TODO: I added a little bit of margin on the frequencies. But this should
% be done better, because now, for very low frequency simulations, the 1e-4
% can be a large number
freq = spec.freq(inbandbins);
F = length(freq);

% the format is the string that contains the input and output format of the inputs and outputs of the signals
% The voltages and currents are in the style V_XXX_pY followed by 'd' or 'c' in the case of differential signals
% The waves are named W_XXX_pY_A and W_XXX_pY_A with again 'd' or 'c' added after the port number
switch type
    case 'S'
        INformat  = 'W_%s_p%s_A';
        OUTformat = 'W_%s_p%s_B';
    case 'Z'
        INformat  = 'I_%s_p%s'; % you never know, maybe someday, I go crazy and implement the whole thing with Y and Z parameters as well
        OUTformat = 'V_%s_p%s';
    case 'Y'
        INformat  = 'V_%s_p%s';
        OUTformat = 'I_%s_p%s';
end

% determine the amount of realisations in the multisine simulation
try
    M = size(spec.(sprintf(INformat,subcircuits{1},'1')),1);
catch
    try
        M = size(spec.(sprintf(INformat,subcircuits{1},'1d')),1);
    catch
        M = size(spec.(sprintf(INformat,subcircuits{1},'1p')),1);
    end
end

% save the total amount of ports for later during the calculation of the distortion covariance matrix
P_total = 0;
% Correct the input and output of each sub-circuit to obtain the Ys
for ss=1:length(subcircuits)
    % P is the amount of ports in the current circuit
    G = BLAs.(subcircuits{ss}).G;
    % interpolate G to the bins of the spectrum
    G = interpolate_MIMO(BLAs.freq,G,freq);
    % determine the amount of ports
    P = size(G,1);
    % don't count TOTAL as one of the stages
    if ~strcmp(subcircuits{ss},'TOTAL')
        P_total = P_total+P;
    end
    % porttype contains 'c' and 'd' for the differential ports
    porttype = BLAs.(subcircuits{ss}).porttype;
    % preallocate the stuff
    Y = zeros(P,M,F);
    U = zeros(P,M,F);
    % gather the input and output signals
    for pp=1:P
        Y(pp,:,:) = squeeze(spec.(sprintf(OUTformat,subcircuits{ss},porttype{pp}))(:,end,inbandbins));
        U(pp,:,:) = squeeze(spec.(sprintf( INformat,subcircuits{ss},porttype{pp}))(:,end,inbandbins));
    end
    % perform the correction
    Ys_all = zeros(P,M,F);
    for ff=1:length(freq)
        for mm=1:M
            Ys_all(:,mm,ff) = Y(:,mm,ff) - G(:,:,ff) * U(:,mm,ff);
        end
    end
    % save the result in the BLAs struct
    BLAs.(subcircuits{ss}).Ys_all = Ys_all;
end

% calculate the covariance matrix for all the stages
for ss=1:length(subcircuits)
    BLAs.(subcircuits{ss}).Cd = calculateCov(BLAs.(subcircuits{ss}).Ys_all);
end

% calculate the covariance matrix of the distortion sources
if nargout>1
    % remove TOTAL from the list of subcircuits, otherwise it ends up in the Cd matrix
    subcircuits = subcircuits(~strcmp(subcircuits,'TOTAL'));
    if uncorr
        temp = cell(1,length(subcircuits));
        for ss=1:length(subcircuits)
            temp{ss} = BLAs.(subcircuits{ss}).Cd;
        end
        Cd = blkdiag_3d(temp);
    else
        % gather all the Ys_all of the stages. ALL in caps, because this matrix is huge!
        Ys_ALL = zeros(P_total,M,F);
        ind=0;
        for ss=1:length(subcircuits)
            P = size(BLAs.(subcircuits{ss}).G,1);
            Ys_ALL(ind+1:ind+P,:,:) = BLAs.(subcircuits{ss}).Ys_all;
            ind = ind+P;
        end
        % and calculate the total covariance matrix
        Cd = calculateCov(Ys_ALL);
    end
end

% if the S-parameters of the TOTAL circuit and the LOAD impedances are provided, calculate the terminated distortion as well
if all(isfield(BLAs,{'LOAD','TOTAL'}))
    BLAs.TERMINATED.Cd = MIMO_BLA_check(BLAs.TOTAL.G,BLAs.TOTAL.Cd,BLAs.LOAD.G);
end

end

function Cd = calculateCov(Ys_all)
% Ys_all is an PxMxF matrix where P signals are measured at F frequencies
% for M different realisations. The result is a PxPxF frequency dependent
% covariance matrix

% P is the amount of ports
[P,~,F] = size(Ys_all);
% preallocate Cd
Cd = zeros(P,P,F);
% and for loops all the way, this could probably be done better with the
% cov function, but I don't trust that one :)
for ff=1:F
    for ii=1:P
        for jj=1:P
            Cd(ii,jj,ff) =  mean( Ys_all(ii,:,ff) .* conj(Ys_all(jj,:,ff)) );
        end
    end
end
end
