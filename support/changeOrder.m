function [res,CvecRes] = changeOrder(M,neworder,CvecM)
% changes the order of an PxPxF matrix to the new order specified in the neworder vector.
%
%   res = changeOrder(M,neworder)
%   [res,CvecRes] = changeOrder(M,neworder,CvecM)
%
% M is a PxPxF matrix that contains an FRM
% neworder is a vector that contains the numbers 1:P, but in a new order
% CvecM is a P*PxP*PxF covariance matrix. This matrix is also changed in order
%
% Adam Cooman, Jan Goos, ELEC VUB

[N,~,F] = size(M);

t = eye(N);
t = t(neworder,:);

% left and right multiply with the transformation matrix
res = zeros(N,N,F);
for ff=1:F
    res(:,:,ff) = t*M(:,:,ff)*t';
end

% if the CvecM is passed, also change its order
if nargin==3
    T = kron(t,t);
    CvecRes = zeros(size(CvecM));
    for ff=1:F
        CvecRes(:,:,ff) = T*CvecM(:,:,ff)*T';
    end
else
    CvecRes = [];
end
    

end