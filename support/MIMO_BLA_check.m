function Cres = MIMO_BLA_check(DEVICE,Cd,LOAD)
% This function terminates an N-port in its load impedances and calculates
% the covariance matrix of the A and B waves at the ports of the system
% when only the noise sources of the N-port are active.
%
%   Cd = MIMO_BLA_check(DEVICE,Cd,LOAD)
%
% DEVICE    is the NxNxF matrix of the device
% Cd        is the NxNxF covariance matrix of the noise sources at the ports of Spd
% LOAD      is the NxNxF matrix which contains the load reflections at the N
%            ports of Spd. Each of the F NxN matrices is a diagonal matrix.
%
% Cres      is the 2*N x 2*N x F covariance matrix of the A nd B waves at the ports
%            of S. First, the A waves are given, then the B-waves in the circuit
%
% Based on the technique described in:
%   J. A. Dobrowolski. "A CAD-Oriented Method for Noise Figure Computation 
%       of Two-Ports with Any Internal Topology" IEEE Tran on Microwave
%       Theory and Techniques, Vol. 37, No. 1, Jan. 1989
% 
% We apply expression (12) where C is given in step b) of the alorithm in
% section III.
%
%
% In any circuit, we can write the following expression:
%       B = S*A + Bn (1)
% where B and A are the waves in a circuit and Bn are sources in the same
% circuit. S is a block diagonal matrix that contains the S-parameters of
% each sub-block.
% Interconnections add relations between the A and the B waves aroud the
% circuit. These interconnections are described using a Gamma matrix:
%       B = Gamma*A  (2)
% The Gamma matrix is mostly zero, but contains 1 at the locations where
% connections are placed. Eliminating B from (1) and (2), we obtain
%       W*A = Bn     (3)
% with
%       W = Gamma - S
% The A-waves in the circuit when it's excited by a vector of B-waves Bn are
% now given by  
%       A = W\Bn      (4)
% Talking about noise, we can only say something about the covariance
% matrix of different signals. But that's just easy:
%       Ca = W\Cb/W
% If we now take Gamma and S and Cb the following matrices:
%           | 0     eye(P) |        | Sdev   0    |      | Cd    0  |
%   Gamma = |              |    S = |             | Cb = |          |
%           | eye(P)  0    |        |  0    Sload |      |  0    0  |
% and calculate Ca, we obtain the covariance matrix of both A and B waves
% in the circuit. For example, if we take a noisy two-port with noiseless
% terminations Rs and Rl
%             -----------
%   ---------|           |----------
%   |   B1<~~|           |<~~A2    |
%  Rs   A1~~>|           |~~>B2    Rl
%   ---------|           |----------
%             -----------
%
% we obtain a Ca which is the correlation matrix of the following vector
%   | A1 |
%   | A2 |
%   | B1 |
%   | B2 |
%
% Adam Cooman, ELEC VUB

[P,~,F] = size(DEVICE);

% do some checks on the inputs
if ~all(size(LOAD)==size(DEVICE))
    error('the load should have the same size as the device.');
end
if ~all(size(Cd)==size(DEVICE))
    error('the distortion covariance matrix should have the same size as the device matrix');
end

% build the big S matrix
S = blkdiag_3d(DEVICE,LOAD);

% build the interconnection matrix
Gamma = [zeros(P) eye(P);eye(P) zeros(P)];

% preallocate the result matrix
Cres = zeros(2*P,2*P,F);
% calculate the waves when only the distortion sources are turned on
for ff=1:F
    Wcurr = Gamma - squeeze(S(:,:,ff));
    Cres(:,:,ff) = Wcurr\[Cd(:,:,ff) zeros(P,P); zeros(P,2*P)]/Wcurr';
end


end