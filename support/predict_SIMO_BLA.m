function SIMO = predict_SIMO_BLA(BLAs)
% predicts the SIMO BLA from the reference to all the waves in the circuit
%
%       SIMO = predict_SIMO_BLA(BLAs)
%
%   BLAs is a struct that should contain the following fields
%       PACKAGE
%       LOAD
%
%
%   The function assumes that the loads are a couple of 1-port impedances
%   which present a reflection factor to the circuit indicated with L1 ... LN. 
%   The BLAs are a bunch of S-matrices that are gatered in a block diagonal
%   matrix. 
%   The package creates the interconnection between the loads and the BLAs.
%
%   It is assumed that a multisine voltage source is connected to the first port.
% 
%                       _______                    _______
%                   ---|_______|---o           ---|_______|---o
%       L1 =       |                   Lx =   |
%                multisine         for x!=1   |
%                __|__                      __|__
%                 ___                        ___ 
%
%
%   The first load impedance is split into a short (Gamma = -1) which is
%   connected to a series impedance L1. The corresponding S-matrix is given
%   by
%
%              1     | -(L+1)    2*(L-1) |
%           -------  |                   |
%             L-3    | 2*(L-1)   -(L+1)  |
%
%   all these matrices are then gathered in a large block diagonal matrix:
%
%       | L1  0  0  0            |
%       | 0  L2  0  0            |
%       | 0  0  ... 0            |
%       | 0  0   0  LN           |
%       |              Pack      |
%       |                   BLAs |
%
%   and the interconnection of the different sub-blocks is represented by a
%   large interconnection matrix Gamma:
% 
%       | zeros(L)   eye(L)                    |
%       |  eye(L)   zeros(L)                   |
%       |                    zeros(P)  eye(P)  |
%       |                     eye(P)  zeros(P) |
%
%   The matrix that describes the sources in the circuit is mainly zero,
%   except for in the first location, where it is equal to 0.5*(1-L1)/sqrt(50)


% check whether the LOAD matrix is diagonal
if ~all(evalFun_3d(BLAs.LOAD.G,@isdiag))
    error('This check will only work with separate loads');
end

% determine the order in which the stages are connected to the package
SystemOrder = unique(cellfun(@(x) x(1:regexp(x,'_p[0-9]+[dcpm]?')-1),BLAs.PACKAGE.portNames,'uniformOutput',false),'stable');

% generate the block-diagonal matrix with the BLAs in it using the correct order
for ii=2:length(SystemOrder)
    if ii==2
        S = BLAs.(SystemOrder{ii}).G;
    else
        S = blkdiag_3d(S,BLAs.(SystemOrder{ii}).G);
    end
end

% L is the amount of loads connected to the circuit
L = size(BLAs.LOAD.G,1);
% P is the amount of internal ports in the circuit
P = size(S,1);
% the size of the package matrix is P+L

% combine all the S-matrices into the big block diagonal one
T = blkdiag_3d( BLAs.LOAD.G , BLAs.PACKAGE.G , S );

%% build the interconnection matrix
Gamma = zeros(size(T,1));
% interconnect the loads to the package
Gamma( (L+1):(L+L) , 1:L ) = eye(L);
% interconnect the BLAs to the package
Gamma( (end-2*P+1):(end-P) , (end-P+1):end ) = eye(P);
% make the Gamma-matrix symmertic
Gamma = Gamma + Gamma.';

% figure(1)
% clf
% spy(Gamma)
% hold on
% spy(T(:,:,10),'r',5)
% spy(ones(size(Gamma)),1)

% Calculate all the A-waves in the system
F = size(T,3);
Aall = zeros(size(Gamma,1),F);
for ff=1:F
    % All A waves are given by inv(Gamma-T)*N
    Aall(:,ff) = (Gamma-T(:,:,ff))\[(1-BLAs.LOAD.G(1,1,ff))/2/sqrt(50);zeros(size(Gamma,1)-1,1)];
end

% get the A and B waves at the ports of the different stages
for pp=1:P
    % BLAs.PACKAGE.portNames
    SIMO.Predicted.(sprintf('W_%s_A',BLAs.PACKAGE.portNames{L+pp})) = Aall( end-P+pp   , : );
    SIMO.Predicted.(sprintf('W_%s_B',BLAs.PACKAGE.portNames{L+pp})) = Aall( end-2*P+pp , : );
end

% get the A and B waves at the output of the total system
for pp=1:L
    SIMO.Predicted.(sprintf('W_%s_A',BLAs.PACKAGE.portNames{pp})) = Aall( end-2*P-L+pp , :);
    SIMO.Predicted.(sprintf('W_%s_B',BLAs.PACKAGE.portNames{pp})) = Aall( end-2*P-2*L+pp , :);
end

% and add the frequency axis
SIMO.Predicted.freq = BLAs.freq;

% also get the measured BLA out of the struct
for ss=1:length(BLAs.Z);
    SIMO.Measured.(BLAs.Z(ss).OutputSignalName) = BLAs.Z(ss).mean;
end
SIMO.Measured.freq = BLAs.Z(1).freq;



end