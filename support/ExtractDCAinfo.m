function [DCAinfo,data] = ExtractDCAinfo(data,diffExPorts)
% EXTRACTDCAINFO looks for the DCA statement in the netlist and extracts its information
%
% [DCAinfo,data] = ExtractDCAinfo(data,diffExPorts)
%
% where data is a cell array of strings which contains the netlist. Use
% ReadTextFile on the netlist to generate the data vector.
% diffExPorts is the 
%
% The function looks for a DCA command in the netlist. for example:
%   DCA:DCA1 port="2" numberOfRealisations=3 display="res"   
%
% The function returns the DCAinfo struct with the following fields:
%   portnr                  port at which the distortion contribution analysis has to
%                           be performed
%   Z0                      characteristic impedance of the waves used in the analysis (default=50)
%   numberOfRealisations    number of realisations of the multisine to be simulated (default=7)
%   display                 what to plot
%   oversample              oversample ratio of the simulation
%   useBLA                  boolean that indicates wheter the real BLA has to be used or just the linear FRF
%   outOfBandFactor         the DCA uses out-of-band BLA, the outOfBandFactor specifies the ratio between MS band and OOB BLA band
%   ticklerDistance         distance (in Hertz) between the main tones and the tickler tones
%   showSteps               boolean to tell the function that it should plot intermediate reults
%   loadOldResults          boolean to indicate whether or not old simulation results should be loaded
%   saveSimResults          boolean to indicate that results should be saved
%
% The function also returns cell array of strings which represents the 
% netlist, but now with the DCA statement removed
% 
% Adam Cooman, ELEC VUB


findstr = '^DCA:\s*[a-zA-Z][a-zA-Z0-9]*\s+';
found = find(cellfun(@(x)~isempty(x),regexp(data,findstr)));
if length(found)==1
    line = data{found};
    
    %% get the port name out of the statement
    t = regexp(line,'port\s*=\s*"(?<value>[a-zA-Z_0-9]+)"(\s|$)','names');
    if ~isempty(t)
        t = regexp(t.value,'(?<number>[1-9][0-9]*)(?<cd>[dc]?)','names');
        if ~isempty(t.number)
            num = str2double(t.number);
            diffports=sum(diffExPorts<num);
            if strcmp(t.cd,'c')
                DCAinfo.portnr = 2*diffports+(num-diffports)+1;
            else
                DCAinfo.portnr = 2*diffports+(num-diffports);
            end
        else
            error('the port number should be provided')
        end
        % translate the port name of the analysis to a port number
    else
        error('you should provide a port name at which the analysis will be provided');
    end
    
    %% Get the characteristic impedance out of the statement
    numpat = '(?<value>[0-9]+(\.[0-9]+)?(e[+\-]?[0-9]+)?)';
    t = regexp(line,['Z0\s*=\s*' numpat '(\s|$)'],'names');
    if ~isempty(t)
        DCAinfo.Z0 = str2double(t.value); % output impedance of the multisine source
    else
        DCAinfo.Z0 = 50;
    end
    
    %% Get the number of realisations out of the statement
    t = regexp(line,'numberOfRealisations\s*=\s*(?<value>[0-9]+)(\s|$)','names');
    if ~isempty(t)
        DCAinfo.numberOfRealisations = str2double(t.value); % output impedance of the multisine source
    else
        DCAinfo.numberOfRealisations = 7;
    end
    
    %% Get the Display out of the statement
    t = regexp(line,'display\s*=\s*"(?<value>[a-zA-Z]+)"(\s|$)','names');
    if ~isempty(t)
        DCAinfo.display = t.value; % output impedance of the multisine source
    else
        DCAinfo.display = 'res';
    end
    
    %% Get the oversample ratio
    t = regexp(line,'oversample\s*=\s*(?<value>[0-9]+)(\s|$)','names');
    if ~isempty(t)
        DCAinfo.oversample = str2double(t.value); % output impedance of the multisine source
    else
        DCAinfo.oversample = 10;
    end
    
    %% Get the useBLA parameter
    t = regexp(line,'useBLA\s*=\s*(?<value>(yes)|(no))(\s|$)','names');
    if ~isempty(t)
        if strcmp(t.value,'yes')
            DCAinfo.useBLA = true;
        else
            DCAinfo.useBLA = false;
        end
    else
        DCAinfo.useBLA = false;
    end
    
    %% get the outOfBandFactor
    t = regexp(line,'outOfBandFactor\s*=\s*(?<value>[0-9]+)(\s|$)','names');
    if ~isempty(t)
        DCAinfo.outOfBandFactor = str2double(t.value); % output impedance of the multisine source
    else
        DCAinfo.outOfBandFactor = 2;
    end
    
    %% get the ticklerDistance
    t = regexp(line,'ticklerDistance\s*=\s*(?<value>[0-9]+)(\s|$)','names');
    if ~isempty(t)
        DCAinfo.ticklerDistance = str2double(t.value); % output impedance of the multisine source
    else
        DCAinfo.ticklerDistance = 1;
    end
    
    %% Get the showSteps parameter
    t = regexp(line,'showSteps\s*=\s*(?<value>(yes)|(no))(\s|$)','names');
    if ~isempty(t)
        if strcmp(t.value,'yes')
            DCAinfo.showSteps = true;
        else
            DCAinfo.showSteps = false;
        end
    else
        DCAinfo.showSteps = false;
    end
    
    %% get the loadOldResults parameter
    t = regexp(line,'loadOldResults\s*=\s*(?<value>(yes)|(no))(\s|$)','names');
    if ~isempty(t)
        if strcmp(t.value,'yes')
            DCAinfo.loadOldResults = true;
        else
            DCAinfo.loadOldResults = false;
        end
    else
        DCAinfo.loadOldResults = false;
    end
    
    %% get the saveSimResults parameter
    t = regexp(line,'saveSimResults\s*=\s*(?<value>(yes)|(no))(\s|$)','names');
    if ~isempty(t)
        if strcmp(t.value,'yes')
            DCAinfo.saveSimResults = true;
        else
            DCAinfo.saveSimResults = false;
        end
    else
        DCAinfo.saveSimResults = false;
    end
    
elseif isempty(found)
    warning('no DCA statement was found in the netlist. using defaults')
else
    error('multiple DCA statements were found in the netlist.')
end



% remove the DCA statement from the netlist
if ~isempty(found)
    data{found}='';
else
    DCAinfo = [];
end

end