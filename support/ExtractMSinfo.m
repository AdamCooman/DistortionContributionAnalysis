function [MSdef,data] = ExtractMSinfo(data)
% EXTRACTMSINFO finds the MS statement in a netlist and generates the corresponding MSdef struct
%
%   [MSdef,data] = ExtractMSinfo(data) 
%
% where data is a cell array of strings which contains the netlist. Use
% ReadTextFile on the netlist to generate the data vector.
%
% The function looks for a MS command in the netlist. for example:
%   MS:MSsource ref 0 fmin=10e6 fmax=1000e6 gridtype="random-odd" amp=0.001
%
% The properties of the MSsource are
%   fmin        minimum excited frequency of the multisine 
%   fmax        maximum excited frequency of the multisine
%   f0          base frequency of the multisine (default = fmin)
%   gridtype    type of frequency grid. options are "full" "odd" or "random-odd" (default = "full")
%   amp         amplitude of one sine in the multisine
%   rout        output impedance of the source (default = 0)
%
% The function returns the MSdef structure corresponding to the multisine
% described in the netlist statement.
% As a second output, the multisine returns the netlist where the MS
% statement is removed.
% 
% Adam Cooman, ELEC VUB

% TODO: use the MScreate function to build the multisine

% TODO: if multiple sources are present, crazy stuff has to happen.

findstr = '^MS:\s*[a-zA-Z][a-zA-Z0-9]*\s+';
found = find(cellfun(@(x)~isempty(x),regexp(data,findstr)));
if length(found)==1
    line = data{found};
    % look for the source name and node(s)
    t = regexp(line,'^MS:\s*(?<sourceName>[a-zA-Z][a-zA-Z0-9]*)\s+(?<node1>[a-zA-Z_0-9]+)\s+(?<node2>[a-zA-Z_0-9]+)','names');
    if ~isempty(t)
        name=t.sourceName;
        MSnode{1} = t.node1;
        MSnode{2} = t.node2;
    else
        error('sourceName and nodes were not found in the MS definition')
    end
    % check the different frequencies
    % for the moment, only X.XeX is supported as numbers. not the fancy stuff with the units
    numpat = '(?<value>[0-9]+(\.[0-9]+)?(e[+\-]?[0-9]+)?)';
    
    % start frequency
    t = regexp(line,['fmin\s*=\s*' numpat '(\s|$)'],'names');
    if ~isempty(t)
        t = t(1);
        fmin = str2double(t.value); % minimum excited frequency in Hz
    else
        error('you should provide a minimum frequency fmin for the multisine')
    end
    % stop frequency
    t = regexp(line,['fmax\s*=\s*' numpat '(\s|$)'],'names');
    if ~isempty(t)
        fmax = str2double(t.value); % maximum excited frequency in Hz
    else
        error('you should provide a maximum frequency fmax for the multisine')
    end
    % grid type
    t = regexp(line,'gridtype\s*=\s*"(?<value>[a-zA-Z\-]+)"(\s|$)','names');
    gridType = t.value;

    % get the amplitude of the components
    t = regexp(line,['amp(litude)?\s*=\s*' numpat '(\s|$)'],'names');
    if ~isempty(t)
        amplitude = str2double(t.value); % Amplitude of the frequency components in the multisine
    else
        error('you should provide an amplitude for the multisine')
    end
    
    % finally, add the output impedance
    t = regexp(line,['rout\s*=\s*' numpat '(\s|$)'],'names');
    if ~isempty(t)
        Rout = str2double(t.value); % output impedance of the multisine source
    else
        Rout = 0; % default is a voltage source
    end
    
    
    
elseif isempty(found)
    error('no multisine source found in the netlist');
else
    error('multiple multisine sources were found in the netlist. this is not supported for the moment.')
end

MSdef = MScreate(fmin,fmax,'grid',gridType,'Rout',Rout,'Amplitude',amplitude);
% don't add the source itself to the netlist, only the subcircuit
MSdef.addSource=0;
MSdef.MSnode = MSnode;
MSdef.name = name;

data{found}='';

end


