function [BLAs,waves,spec,wavesraw,specraw] = DCA_estimate_BLAs(Netlist,MSdef,varargin)
% DCA_estimate_BLAs determines the Best Linear Approximation of each marked sub-circuit
% 
%      [BLAs,waves,spec,wavesraw,specraw] = DCA_estimate_BLAs(Netlist,MSdef);
%      [BLAs,waves,spec,wavesraw,specraw] = DCA_estimate_BLAs(Netlist,MSdef,'ParamName',paramValue,...);
% 
% 
% Required inputs:
% - Netlist      check: @(x) ischar(x)|iscellstr(x)
%      ADS netlist that describes the circuit. The borders of sub-circuits
%      are  indicated with current probes that are named I_XXX_pY where
%      XXX is the  name of the sub-circuit and Y is the number of the
%      port. The current probes point into the sub-circuit  The total
%      circuit is denoted with current probes called I_TOTAL_pY
% - MSdef      check: @isstruct
%      Multisine used to excite the circuit. This multisine struct
%      can easily be  created by the MScreate function. It requires
%      an MSnode field additionaly  to indicate where the multisine
%      is connected to
% 
% Parameter/Value pairs:
% - 'technique'     default: 'Sparam'  check: @(x) any(strcmpi(x,{'Sparam','LSSS','tickle'}))
%      technique used to estimate the MIMO BLAs Sparam and AC assume
%      the BLA is  equal to the small-singal FRM and estimate the small-signal
%      S-parameters  using an AC or S-parameter simulation to determine
%      these. 'LSSS' uses a  large-signal small-signal around the multisine
%      and 'tickle' uses tickle  multisines to perform the estimation
% - 'estimateTotal'     default: true  check: @islogical
%      indicates whether the total system is also estimated
% - 'unbal2bal'     default: true  check: @islogical
%      if this is true, the sub-circuits with differential ports are
%      converted  to mixed-mode S-parameters
% - 'useOldResults'     default: false  check: @islogical
%      if this is set to true, the function looks for old simulation
%      results, to  avoid doing extra simulations
% - 'oversample'     default: 1  check: @isscalar
%      estimated orded of non-linearity of the system. This determines
%      the  amount of frequency bins in the simulation
% - 'Z0'     default: 50  check: @isscalar
%      characteristic impedance used for the waves. For differential
%      ports,  this impedance is double while for common-mode ports
%      it is halved
% - 'cleanup'     default: true  check: @islogical
%      indicates whether or not the temporary files should be cleaned
%      up after  the simulations. Usefull for debugging purposes
% 
% Outputs:
% - BLAs      Type: struct 
%      struct that contains the Best Linear Approximation of each of
%      the sub-circuits. It contains a field that has the same name
%      as the sub-circtuit. Each of these fields contains a field G
%      which holds the BLA, and a field  Then three more fields are
%      added: one for the TOTAL circuit, one for the PACKAGE, which
%      contains the S-matrix of the interconnection network between
%      the stages and one for the LOAD which contains the S-matrix
%      seen as a load from the ports of the TOTAl circuit. 
% - waves      Type: struct
%      struct that contains the waves of the system under steady stage
%      for different phase realisation of the input multisine. If the
%      BLA is approximated by the small-signal behaviour, this struct
%      is empty
% - spec      Type: struct
%      struct that contains the voltages and currents of the circuit
%      in steady state.
% 
% Example:
%   BLAs
%     XXX         - field for each sub-circuit
%         XXX.G               BLA of the sub-circuit
%         XXX.portType        cell array of strings that contains the portType
%     PACKAGE     field for the information about the package
%         PACKAGE.G           S-parameters of the package
%         PACKAGE.portNames   
%     TOTAL       - field for the total circuit
%         TOTAL.G             BLA of the total circuit
%         TOTAL.G_conn        FRM obtained for the total circuit by interconnecting 
%                             all the sub-circuits and the package
%         TOTAL.portNames     
%     LOAD        - S-matrix of the load seen from the total circuit  
%         LOAD.G              FRM with the info
%     freq        - frequency axis of the whole thing
% 
% 
% This documentation was generated with the generateFunctionHelp function at 05-May-2015


p = inputParser();
p.KeepUnmatched = true;
% ADS netlist that describes the circuit. The borders of sub-circuits are
% indicated with current probes that are named I_XXX_pY where XXX is the
% name of the sub-circuit and Y is the number of the port. The current probes point into the sub-circuit
% The total circuit is denoted with current probes called I_TOTAL_pY
p.addRequired('Netlist',@(x) ischar(x)|iscellstr(x));
% Multisine used to excite the circuit. This multisine struct can easily be
% created by the MScreate function. It requires an MSnode field additionaly
% to indicate where the multisine is connected to
p.addRequired('MSdef',@isstruct);
% technique used to estimate the MIMO BLAs Sparam and AC assume the BLA is
% equal to the small-singal FRM and estimate the small-signal S-parameters
% using an AC or S-parameter simulation to determine these. 'LSSS' uses a
% large-signal small-signal around the multisine and 'tickle' uses tickle
% multisines to perform the estimation
p.addParamValue('technique','Sparam',@(x) any(strcmpi(x,{'Sparam','LSSS','FOMS','ZIPPER','SUBSTRACT'})));
% indicates whether the total system is also estimated
p.addParamValue('estimateTotal',true,@islogical);
% if this is true, the sub-circuits with differential ports are converted
% to mixed-mode S-parameters
p.addParamValue('unbal2bal',true,@islogical);
% if this is set to true, the function looks for old simulation results, to
% avoid doing extra simulations
p.addParamValue('useOldResults',false,@islogical);
% estimated order of non-linearity of the system. This determines the
% amount of frequency bins in the simulation
p.addParamValue('oversample',1,@isscalar);
% characteristic impedance used for the waves. For differential ports,
% this impedance is double while for common-mode ports it is halved
p.addParamValue('Z0',50,@isscalar);
% indicates whether or not the temporary files should be cleaned up after
% the simulations. Usefull for debugging purposes
p.addParamValue('cleanup',true,@islogical);
% the number of realisations used in the estimation of the best linear
% approximation
p.addParameter('numberOfRealisations',3,@isnatural);
% maximum crest factor of the multisines in the simulations
p.addParameter('CF',Inf,@isscalar);
p.parse(Netlist,MSdef,varargin{:});
args = p.Results;
clear Netlist MSdef


% if the netlist is a filename, read its contents
if ischar(args.Netlist)
    args.Netlist = readTextFile(args.Netlist);
end

switch upper(args.technique)
    case 'SPARAM'
        %% Just estimate the small-signal FRFs
        % add an AC source at the location of the multisine source with the correct output impedance if the source has an output impedance, just add a resistor.
        args.Netlist = replaceMS(args.Netlist,args.MSdef);
        % determine the S-parameters of the package and of the stages using an S-parameter simulation
        BLAs = Linear_Analysis_v2(args.Netlist , ...
            'fstart', args.MSdef.fmin , 'fstop', args.oversample*args.MSdef.f0*max(args.MSdef.grid), 'fstep', args.MSdef.f0,...
            'Z0',args.Z0,'cleanup',args.cleanup,'unbal2bal',args.unbal2bal);
        % also determine the S-parameters of the total circuit and of the load impedances seen by the circuit
        TOTAL = Linear_Analysis_v2(args.Netlist , ...
            'fstart', args.MSdef.fmin , 'fstop', args.oversample*args.MSdef.f0*max(args.MSdef.grid), 'fstep', args.MSdef.f0,...
            'Z0',args.Z0,'cleanup',args.cleanup,'unbal2bal',args.unbal2bal,'onlyTotal',true,'getLoadImps',true);
        BLAs.TOTAL = TOTAL.TOTAL;
        BLAs.LOAD = TOTAL.LOAD;
        % interconnect the blocks, to obtain the BLA of the total system in BLAs.TOTAL.G_conn
        BLAs = interconnectStages(BLAs);
        % during linear analysis, no phase realisations of the multisine are calculated
        waves = []; spec = []; wavesraw = []; specraw = [];
    case 'LSSS'
        %% determine the BLA with an LSSS simulation
        error('adding LSSS analysis is still a big TODO');
    case 'FOMS'
        %% use orthogonal multisines to perform the BLA estimation
        error('adding FMOS tickler analysis is still a big TODO');
    case 'ZIPPER'
        %% determine the BLA using zippered multisines
        % run a zipper analysis of the circuit
        [BLAs,spec,waves,specraw,wavesraw] = Zipper_Analysis(args.Netlist,args.MSdef,...
            'numberOfRealisations',args.numberOfRealisations,'CF',args.CF,...
            'unbal2bal',args.unbal2bal,p.Unmatched);
        
        % run a linear analysis as well, to get something to compare to
        args.Netlist = replaceMS(args.Netlist,args.MSdef);
        LIN = Linear_Analysis_v2(args.Netlist , ...
            'fstart', args.MSdef.fmin , 'fstop', args.oversample*args.MSdef.f0*max(args.MSdef.grid), 'fstep', args.MSdef.f0,...
            'Z0',args.Z0,'cleanup',args.cleanup,'unbal2bal',args.unbal2bal);
        % the package S-matrix can be found in this linear simulation
        BLAs.PACKAGE = LIN.PACKAGE;LIN = rmfield(LIN,'PACKAGE');
        % add the G_linear field to all the BLAs
        BLAs.freq_lin = LIN.freq;LIN = rmfield(LIN,'freq');
        stages = fieldnames(LIN);
        for ss=1:length(stages)
            BLAs.(stages{ss}).G_linear = LIN.(stages{ss}).G;
        end
        % TODO: portType should be added to the structs
        TOTAL = Linear_Analysis_v2(args.Netlist , ...
            'fstart', args.MSdef.fmin , 'fstop', args.oversample*args.MSdef.f0*max(args.MSdef.grid), 'fstep', args.MSdef.f0,...
            'Z0',args.Z0,'cleanup',args.cleanup,'unbal2bal',args.unbal2bal,'onlyTotal',true,'getLoadImps',true);
        BLAs.TOTAL.G_linear = TOTAL.TOTAL.G;
        BLAs.LOAD = TOTAL.LOAD;
        
    case 'SUBSTRACT'
        %% first simulate the large spectrum on its own, then with ticklers and substract the response
        [BLAs,spec,waves] = Substract_Analysis(args.Netlist,args.MSdef,...
            'numberOfRealisations',args.numberOfRealisations);
        
        
        
end


end


function Netlist = replaceMS(Netlist,MSdef)
% if the nodes are not in a cell array, add ground as a second connection
if ~iscell(MSdef.MSnode)
    MSdef.MSnode = {MSdef.MSnode '0'};
end

if MSdef.Rout==0
%     Netlist{end+1} = ADSaddShort(-1,'MSshort','sink',MSdef.MSnode{1},'source',MSdef.MSnode{2},'Mode',0,'SaveCurrent','no');
    Netlist{end+1} = ADSaddV_Source(-1,'MSshort','p',MSdef.MSnode{1},'n',     MSdef.MSnode{2},'Vdc',MSdef.DC,'Vac',0,'SaveCurrent','no');
elseif MSdef.Rout==Inf
    Netlist{end+1} = ADSaddI_Source(-1,'MSshort','source',MSdef.MSnode{1},'sink',     MSdef.MSnode{2},'Idc',MSdef.DC,'Vac',0,'SaveCurrent','no');
else
    Netlist{end+1} = ADSaddV_Source(-1,'MS_DC_source','p','MSinternalNode','n',     MSdef.MSnode{2},'Vdc',MSdef.DC,'Vac',0,'SaveCurrent','no');
    Netlist{end+1} = ADSaddR(-1,'R_MS_ac','p',MSdef.MSnode{1},'n','MSinternalNode','R',MSdef.Rout);
end

end

% @generateFunctionHelp
% @tagline determines the Best Linear Approximation of each marked sub-circuit

% @output1 struct that contains the Best Linear Approximation of each of
% @output1 the sub-circuits. It contains a field that has the same name as the
% @output1 sub-circtuit. Each of these fields contains a field G which holds the
% @output1 BLA, and a field 
% @output1 Then three more fields are added: one for the TOTAL circuit, one for the
% @output1 PACKAGE, which contains the S-matrix of the interconnection network
% @output1 between the stages and one for the LOAD which contains the S-matrix seen
% @output1 as a load from the ports of the TOTAl circuit. 
% @outputType1 struct 

% @output2 struct that contains the waves of the system under steady stage
% @output2 for different phase realisation of the input multisine. If the BLA is
% @output2 approximated by the small-signal behaviour, this struct is empty
% @outputType2 struct

% @output3 struct that contains the voltages and currents of the circuit in
% @output3 steady state.
% @outputType3 struct

% @example BLAs
% @example   XXX         - field for each sub-circuit
% @example       XXX.G               BLA of the sub-circuit
% @example       XXX.portType        cell array of strings that contains the portType
% @example   PACKAGE     field for the information about the package
% @example       PACKAGE.G           S-parameters of the package
% @example       PACKAGE.portNames   
% @example   TOTAL       - field for the total circuit
% @example       TOTAL.G             BLA of the total circuit
% @example       TOTAL.G_conn        FRM obtained for the total circuit by interconnecting 
% @example                           all the sub-circuits and the package
% @example       TOTAL.portNames     
% @example   LOAD        - S-matrix of the load seen from the total circuit  
% @example       LOAD.G              FRM with the info
% @example   freq        - frequency axis of the whole thing
%% generateFunctionHelp: old help, backed up at 05-May-2015. leave this at the end of the function
% DCA_estimate_BLAs determines the Best Linear Approximation of each marked sub-circuit
% 
%      [BLAs,waves,spec] = DCA_estimate_BLAs(Netlist,MSdef);
%      [BLAs,waves,spec] = DCA_estimate_BLAs(Netlist,MSdef,'ParamName',paramValue,...);
% 
% 
% Required inputs:
% - Netlist      check: @(x) ischar(x)|iscellstr(x)
%      ADS netlist that describes the circuit. The borders of sub-circuits
%      are  indicated with current probes that are named I_XXX_pY where
%      XXX is the  name of the sub-circuit and Y is the number of the
%      port. The current probes point into the sub-circuit  The total
%      circuit is denoted with current probes called I_TOTAL_pY
% - MSdef      check: @isstruct
%      Multisine used to excite the circuit. This multisine struct
%      can easily be  created by the MScreate function. It requires
%      an MSnode field additionaly  to indicate where the multisine
%      is connected to
% 
% Parameter/Value pairs:
% - 'technique'     default: 'Sparam'  check: @(x) any(strcmpi(x,{'Sparam','LSSS','tickle'}))
%      technique used to estimate the MIMO BLAs Sparam and AC assume
%      the BLA is  equal to the small-singal FRM and estimate the small-signal
%      S-parameters  using an AC or S-parameter simulation to determine
%      these. 'LSSS' uses a  large-signal small-signal around the multisine
%      and 'tickle' uses tickle  multisines to perform the estimation
% - 'estimateTotal'     default: true  check: @islogical
%      indicates whether the total system is also estimated
% - 'unbal2bal'     default: true  check: @islogical
%      if this is true, the sub-circuits with differential ports are
%      converted  to mixed-mode S-parameters
% - 'useOldResults'     default: false  check: @islogical
%      if this is set to true, the function looks for old simulation
%      results, to  avoid doing extra simulations
% - 'oversample'     default: 1  check: @isscalar
%      estimated orded of non-linearity of the system. This determines
%      the  amount of frequency bins in the simulation
% - 'Z0'     default: 50  check: @isscalar
%      characteristic impedance used for the waves. For differential
%      ports,  this impedance is double while for common-mode ports
%      it is halved
% - 'cleanup'     default: true  check: @islogical
%      indicates whether or not the temporary files should be cleaned
%      up after  the simulations. Usefull for debugging purposes
% 
% Outputs:
% - BLAs      Type: struct 
%      struct that contains the Best Linear Approximation of each of
%      the sub-circuits. It contains a field that has the same name
%      as the sub-circtuit. Each of these fields contains a field G
%      which holds the BLA, and a field  Then three more fields are
%      added: one for the TOTAL circuit, one for the PACKAGE, which
%      contains the S-matrix of the interconnection network between
%      the stages and one for the LOAD which contains the S-matrix
%      seen as a load from the ports of the TOTAl circuit. 
% - waves      Type: struct
%      struct that contains the waves of the system under steady stage
%      for different phase realisation of the input multisine. If the
%      BLA is approximated by the small-signal behaviour, this struct
%      is empty
% - spec      Type: struct
%      struct that contains the voltages and currents of the circuit
%      in steady state.
% 
% BLAs   XXX         - field for each sub-circuit       XXX.G               BLA of
% the sub-circuit       XXX.portType        cell array of strings that contains the
% portType   PACKAGE     field for the information about the package       PACKAGE.G
%           S-parameters of the package       PACKAGE.portNames      TOTAL       -
% field for the total circuit       TOTAL.G             BLA of the total circuit 
%      TOTAL.G_conn        FRM obtained for the total circuit by interconnecting 
%                           all the sub-circuits and the package       TOTAL.portNames
%        LOAD        - S-matrix of the load seen from the total circuit         LOAD.G
%              FRM with the info   freq        - frequency axis of the whole thing
% 
% 
% This documentation was generated with the generateFunctionHelp function at 05-May-2015
