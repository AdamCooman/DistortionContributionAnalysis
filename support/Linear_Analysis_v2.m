function BLAs = Linear_Analysis_v2(NetlistFile,varargin)
% Linear_Analysis_v2 determines the S-matrix of the stages and the package automatically
% 
%      BLAs = Linear_Analysis_v2(NetlistFile);
%      BLAs = Linear_Analysis_v2(NetlistFile,'ParamName',paramValue,...);
% 
% The ports of the stages and system are indicated with current probes pointing into
% the stage.  The current probe should have the following name: I_STAGENAME_py Where
% STAGENAME indicates the name of the stage and y indicates the port number. For the
% ports of the total system, use "TOTAL" as stagename. If an input is differential,
% add 'p' and 'm' to the port number to indicate the plus and the minus port respectively.
% 
% 
% Required inputs:
% - NetlistFile      check: @(x) iscellstr(x)||ischar(x)
%      ADS netlist that holds the circuit. It should also contain several
%       current probes with a name I_XXX_pY where XXX is the name of
%      the stage  under consideration.  Make sure you replace any multisine
%      sources by their output impedance, to  have the correct DC operating
%      point.
% 
% Parameter/Value pairs:
% - 'fstart'     default: 0  check: @isscalar
%      start frequency of the S-parameter simulation
% - 'fstop'     default: []  check: @isscalar
%      stop frequency of the S-parameter simulation
% - 'fstep'     default: []  check: @isscalar
%      frequency step used in the S-parameter simulation
% - 'Z0'     default: 50  check: @isscalar
%      reference impedance of the S-parameters
% - 'unbal2bal'     default: true  check: @islogical
%      boolean that indiactes to use conversion to mixed-mode or not
% - 'Display'     default: 0  check: @islogical
%      to plot, or not to plot?
% - 'onlyInterconnect'     default: false  check: @islogical
%      If only the interconnection has to be calculated, you can set
%      this boolean to true if you only need Sp
% - 'cleanup'     default: true  check: @islogical
%      if this is false, the temporary files are not deleted after
%      the simulation
% - 'onlyTotal'     default: false  check: @islogical
%      if this is true, only the S-parameters of the total system are
%      determined
% - 'getLoadImps'     default: false  check: @islogical
%      indicate whether you want to determine the load impedances as
%      well
% 
% Outputs:
% - BLAs      Type: struct
%      contains the S-parameters of each stage in a field that has
%      the name of the stage in the circuit. Besides, it contains a
%      PACKAGE field which holds the S-parameters of the package and
%      it can contain a LOAD field that contains the S-parameters of
%      the seen from the inputs of the total system.
% 
% Example:
%   Here are some examples of port names:
%     The current probe at the second port of the INPUT stage should be called I_INPUT_p2. 
%     The current probe at first external port should be called I_TOTAL_p1.
%     The current probe at the differential port should be called I_AMP_p1p
%     and the second port, which is the negative input should be called I_AMP_p1m
% 
% Zref is a reserved variable that is added by this function
% 
% Adam Cooman, ELEC VUB
% 
% 
% This documentation was generated with the generateFunctionHelp function at 01-Jun-2015

% get all the inputs parsed!
p = inputParser();
% ADS netlist that holds the circuit. It should also contain several
% current probes with a name I_XXX_pY where XXX is the name of the stage
% under consideration. 
% Make sure you replace any multisine sources by their output impedance, to
% have the correct DC operating point.
p.addRequired('NetlistFile',@(x) iscellstr(x)||ischar(x));
% start frequency of the S-parameter simulation
p.addParamValue('fstart',0,@isscalar);
% stop frequency of the S-parameter simulation
p.addParamValue('fstop',[],@isscalar);
% frequency step used in the S-parameter simulation
p.addParamValue('fstep',[],@isscalar);
% reference impedance of the S-parameters
p.addParamValue('Z0',50,@isscalar);
% boolean that indiactes to use conversion to mixed-mode or not
p.addParamValue('unbal2bal',true,@islogical);
% to plot, or not to plot?
p.addParamValue('Display',0,@islogical);
% If only the interconnection has to be calculated, you can set this boolean to true if you only need Sp
p.addParamValue('onlyInterconnect',false,@islogical);
% if this is false, the temporary files are not deleted after the simulation
p.addParamValue('cleanup',true,@islogical)
% if this is true, only the S-parameters of the total system are determined
p.addParamValue('onlyTotal',false,@islogical)
% indicate whether you want to determine the load impedances as well
p.addParamValue('getLoadImps',false,@islogical);
% multisine source. If you add this as a parameter, the multisine source
% will be represented in the circuit by its impedance and possible DC offset
p.addParamValue('MSdef',[],@isstruct);
% parse the input
p.parse(NetlistFile,varargin{:});
args = p.Results();
clear NetlistFile fstart fstop fstep varargin p

if isempty(args.fstop)
    error('you should provide fstart, fstop and fstep to the function');
end

% check the start and the stop frequency
if args.fstart>args.fstop
    error('Start frequency is higher than the stop frequency')
end

if ischar(args.NetlistFile)
    % read the file into a cell array
    data = readTextFile(args.NetlistFile);
else
    % the data has been read already, just use the netlist
    if iscell(args.NetlistFile)
        data = args.NetlistFile;
    end
end

% if the multisine is provided, replace it by its impedance an DC offset
if ~isempty(args.MSdef);
    data = replaceMS(data,args.MSdef);
end

%% massage the netlist a little bit, place the terminations in the correct locations

% make sure the voltage at each current source is measured
data = prepareWaveSimulation(data);
% analyse the netlist to obtain the amount of ports for each stage in the netlist
[Extern,Stages,StageNames,externNodes,stageNodes,diffExPorts,diffStPorts] = analyseNetlist(data);

% if only the total circuit is demanded, remove all the other stages from the list
if args.onlyTotal
    findstr = '^\s*Short\s*\:\s*I_(?<stagename>TOTAL)_p(?<portnr>[1-9][0-9]*)(?<diff>[pm]?)\s+(?<node1>[a-zA-Z0-9_]+)\s+(?<node2>[a-zA-Z0-9_]+)\s';
    Stages = [];
    StageNames = {};
    diffStPorts = {};
else
    findstr = '^\s*Short\s*\:\s*I_(?<stagename>[a-zA-Z][a-zA-Z0-9]*)_p(?<portnr>[1-9][0-9]*)(?<diff>[pm]?)\s+(?<node1>[a-zA-Z0-9_]+)\s+(?<node2>[a-zA-Z0-9_]+)\s';
end

% now replace the ports with the appropriate separation block.
outnum=1;
intnum=1;
for ii=1:length(data)
    t = regexp(data{ii},findstr,'names');
    if ~isempty(t)
        if strcmp(t.stagename,'TOTAL')
            % OutsideInterface:OUT6  in N__28 P1=1 Zref=Zref
            currport = str2double(t.portnr);
            % find the amount of differential ports that has a portnumber smaller than the current one
            diffports=sum(diffExPorts<currport);
            portnr = 2*diffports+(currport-diffports);
            % if the port is the + port of a differential one, the port
            % number is correct already. If it's the minus port, add 1
            if strcmp(t.diff,'m')
                portnr = portnr+1;
            end
            if args.getLoadImps
                % if the load impedances are wanted, put them at the very end of the matrix
                
                data{ii}=sprintf('StageInterface:OUT%d %s %s P1=%d P2=%d Zref=Zref',outnum,t.node1,t.node2,Extern+2*sum(Stages)+portnr,portnr);
            else
                data{ii}=sprintf('OutsideInterface:OUT%d %s %s P1=%d Zref=Zref',outnum,t.node1,t.node2,portnr);
            end
            % outnum is just used to keep track of the amount of subcomponents we added
            outnum=outnum+1;
        else
            % find the number of the stage in the list
            stagenr = find(not(cellfun('isempty',regexp(StageNames,['^' t.stagename '$']))),1);
            % the port number is in the name of the current probe
            currport = str2double(t.portnr);
            % find the amount of differential ports with a number smaller than the port number
            diffports=sum(diffStPorts{stagenr}<currport);
            % now get the actual port number of our port
            portnr = 2*diffports+(currport-diffports);
            % if it's the minus port of a differential port, add one.
            if strcmp(t.diff,'m')
                portnr = portnr+1;
            end
            % calculate the port in the package the stage is connected to
            packageport = Extern + sum(Stages(1:stagenr-1)) + portnr;
            % calculate the number of the port in the big S-matrix
            stageport = Extern + sum(Stages) + sum(Stages(1:stagenr-1)) + portnr;
            % add the interface component: "StageInterface:INT2  N__59 V_s2_p2 P1=6 P2=11 Zref=Zref"
            if ~args.onlyInterconnect
                data{ii}=sprintf('StageInterface:INT%s %s %s P1=%s P2=%s Zref=Zref',num2str(intnum),t.node1,t.node2,num2str(packageport),num2str(stageport));
            else
                data{ii}=sprintf('StageInterface:INT%s %s %s P1=%s Zref=Zref',num2str(intnum),t.node1,t.node2,num2str(packageport));
            end
            % intnum is just used to keep track of the amount of subcomponents we added
            intnum=intnum+1;
        end
    end
end

% add the definition of the interface blocks
data = AddInterfaceBlocks(data,args.onlyInterconnect);
% add the characteristic impedance Zref used at all the ports
data{end+1} = sprintf('Zref=%s ',num2str(args.Z0));
% add the S-paramter simulation statement
data{end+1} = ADSaddS_Param(0,'SPARAM','SweepVar','freq','SweepPlan','SP_sweep');
% add the frequency sweep
data{end+1} = ADSaddSweepPlan(0,'SP_sweep','Start',args.fstart,'Stop',args.fstop,'Step',args.fstep);
% add an options statement to perform a decent topology check
data{end+1} = ADSaddOptions(0,'Options1','TopologyCheck','yes','TopologyCheckMessages','yes','GiveAllWarnings','yes');
% write everything to a temporary file
time = clock;
tempNetlistFile = ['Linear_Analysis_' num2str(time(1)) '_' num2str(time(2)) '_' num2str(time(3)) '_' num2str(time(4)) '_' num2str(time(5)) '_' num2str(round(time(6))) '.net'];
writeTextFile(data,tempNetlistFile);

%% Run the actual simulation
try
    simres = ADSrunSimulation(tempNetlistFile);
    % descend one level into the result struct
    simres = simres.sim1_S;
catch err
    error(['simulation failed. with the following message: ' err.message]);
end
% clean up all the rubbish
if args.cleanup
    delete(tempNetlistFile);
end

%% process the simulation results

% cut away the extra part of the S matrix that was added to get the load impedances
if args.getLoadImps
    BLAs.LOAD.G = simres.S(Extern+2*sum(Stages)+1:end,Extern+2*sum(Stages)+1:end,:);
    simres.S = simres.S(1:Extern+2*sum(Stages),1:Extern+2*sum(Stages),:);
end

% if only the total circuit is required, the simulation result contains only the s-parameters of the total circuit
if args.onlyTotal
    BLAs.TOTAL.G = simres.S;
end
    
% get the package S-matrix from the simresult
pp = Extern+sum(Stages);
BLAs.PACKAGE.G  = simres.S( 1:pp , 1:pp , : );
% and throw away this part of the simres matrix
simres.S = simres.S(pp+1:end,pp+1:end,:);

% Generate the names of the sub-circuits connected to the package S-matrix
BLAs.PACKAGE.portNames = {};
for pp=1:Extern-length(diffExPorts)
    if any(pp==diffExPorts);
        if args.unbal2bal
            BLAs.PACKAGE.portNames{end+1} = sprintf('TOTAL_p%dd',pp);
            BLAs.PACKAGE.portNames{end+1} = sprintf('TOTAL_p%dc',pp);
        else
            BLAs.PACKAGE.portNames{end+1} = sprintf('TOTAL_p%dp',pp);
            BLAs.PACKAGE.portNames{end+1} = sprintf('TOTAL_p%dm',pp);
        end
    else
        BLAs.PACKAGE.portNames{end+1} = sprintf('TOTAL_p%d',pp);
    end
end
for ss=1:length(Stages)
    for pp=1:Stages(ss)-length(diffStPorts{ss})
        if any(pp==diffStPorts{ss});
            if args.unbal2bal
                BLAs.PACKAGE.portNames{end+1} = sprintf('%s_p%dd',StageNames{ss},pp);
                BLAs.PACKAGE.portNames{end+1} = sprintf('%s_p%dc',StageNames{ss},pp);
            else
                BLAs.PACKAGE.portNames{end+1} = sprintf('%s_p%dp',StageNames{ss},pp);
                BLAs.PACKAGE.portNames{end+1} = sprintf('%s_p%dm',StageNames{ss},pp);
            end
        else
            BLAs.PACKAGE.portNames{end+1} = sprintf('%s_p%d',StageNames{ss},pp);
        end
    end
end

% put the S-parameters of each stage in the BLAs struct as well
for ss=1:length(Stages)
    index = sum(Stages(1:ss-1))+1:sum(Stages(1:ss));
    BLAs.(StageNames{ss}).G = simres.S(index,index,:);
end

%% transform the S-parameters to mixed-mode S-parameters when differential ports are present.
if args.unbal2bal
    % now apply the unbalanced to balanced transform to all stages
    for ss=1:length(Stages)
        BLAs.(StageNames{ss}).G = unbalanced_to_balanced(BLAs.(StageNames{ss}).G,diffStPorts{ss},args.Z0);
    end
    
    % and convert the package to differential signals as well
    % find the numbers of the differential ports on the package S-matrix
    diffPortsPackage = diffExPorts;
    diffportspassed=0;
    for ss=1:length(Stages)
        diffPortsPackage = [diffPortsPackage Extern-length(diffExPorts)+sum(Stages(1:ss-1))-diffportspassed+diffStPorts{ss}];%#ok
        diffportspassed = diffportspassed + length(diffStPorts{ss});
    end
    BLAs.PACKAGE.G = unbalanced_to_balanced(BLAs.PACKAGE.G,diffPortsPackage,args.Z0);
    
    % the S-parameters of the TOTAL circuit should be converted ti differential as well
    if isfield(BLAs,'TOTAL')
        BLAs.TOTAL.G = unbalanced_to_balanced(BLAs.TOTAL.G,diffExPorts,args.Z0);
    end
    
    % the LOAD should maybe be converted into differential as well
    if isfield(BLAs,'LOAD')
        BLAs.LOAD.G = unbalanced_to_balanced(BLAs.LOAD.G,diffExPorts,args.Z0);
    end
else
    diffPortsPackage=[];
end


%% save the port type in the struct as well, to make indicate the differential and common-mode ports
for ss=1:length(StageNames)
    BLAs.(StageNames{ss}).porttype = savePortType(Stages(ss),diffStPorts{ss}, args.unbal2bal );
end
BLAs.PACKAGE.porttype = savePortType(Extern+sum(Stages),diffPortsPackage , args.unbal2bal);
if isfield(BLAs,'TOTAL')
    BLAs.TOTAL.porttype = savePortType( Extern , diffExPorts , args.unbal2bal );
end
if isfield(BLAs,'LOAD')
    BLAs.LOAD.porttype = savePortType( Extern , diffExPorts , args.unbal2bal );
end

%% add the frequency axis to the simulation results
BLAs.freq = simres.freq;


end


function data = AddInterfaceBlocks(data,onlyInterconnect)
% add the definition of StageInterface to the netlist
if ~onlyInterconnect
    %
    %  o------DC_BLOCK-----o
    %     |             |
    %   DC_FEED      DC_FEED
    %     |             |
    %   Term1         Term2
    %     |             |
    %    gnd           gnd
    %
    data{end+1}='define StageInterface ( N__4  N__5 )';
    data{end+1}='parameters  P1=1  P2=2  Zref=50';
    data{end+1}='Short:FEED  N__4 N__5 Mode=-1';
    data{end+1}='Short:BLOCK1  N__4 N__2 Mode=1';
    data{end+1}='Port:Term1  N__2 0 Num=P1 Z=Zref Noise=yes';
    data{end+1}='Short:BLOCK2  N__5 N__3 Mode=1';
    data{end+1}='Port:Term2  N__3 0 Num=P2 Z=Zref Noise=yes';
    data{end+1}='end StageInterface';
else
    % if only the interconnect is needed, don't put a port on the inside of the DC_feed
    %
    %  o------DC_BLOCK-----o
    %     |            
    %   DC_FEED      
    %     |            
    %   Term1         
    %     |           
    %    gnd          
    %
    data{end+1}='define StageInterface ( N__4  N__5 )';
    data{end+1}='parameters  P1=1  Zref=50';
    data{end+1}='Short:FEED  N__4 N__5 Mode=-1'; 
    data{end+1}='Short:BLOCK1  N__4 N__2 Mode=1';
    data{end+1}='Port:Term1  N__2 0 Num=P1 Z=Zref Noise=yes';
    data{end+1}='end StageInterface';
end

% Add the definition of the outsideInterface to the end
%  o------DC_BLOCK-----o
%                   |
%                 DC_FEED
%                   |
%                 Term
%                   |
%                  gnd
%
% N__6 is connected to the outside, N__7 is connected to the inside of the circuit under test
data{end+1}='define OutsideInterface ( N__6  N__7 )';
data{end+1}='parameters  P1=1  Zref=50';
data{end+1}='Short:FEED  N__6 N__7 Mode=-1';
data{end+1}='Short:BLOCK  N__7 N__2 Mode=1';
data{end+1}='Port:Term  N__2 0 Num=P1 Z=Zref Noise=yes';
data{end+1}='end OutsideInterface';
end

function Netlist = replaceMS(Netlist,MSdef)
% if the nodes are not in a cell array, add ground as a second connection
if ~iscell(MSdef.MSnode)
    MSdef.MSnode = {MSdef.MSnode '0'};
end

if MSdef.Rout==0
%     Netlist{end+1} = ADSaddShort(-1,'MSshort','sink',MSdef.MSnode{1},'source',MSdef.MSnode{2},'Mode',0,'SaveCurrent','no');
    Netlist{end+1} = ADSaddV_Source(-1,'MSshort','p',MSdef.MSnode{1},'n',     MSdef.MSnode{2},'Vdc',MSdef.DC,'Vac',0,'SaveCurrent','no');
elseif MSdef.Rout==Inf
    Netlist{end+1} = ADSaddI_Source(-1,'MSshort','source',MSdef.MSnode{1},'sink',     MSdef.MSnode{2},'Idc',MSdef.DC,'Vac',0,'SaveCurrent','no');
else
    Netlist{end+1} = ADSaddV_Source(-1,'MS_DC_source','p','MSinternalNode','n',     MSdef.MSnode{2},'Vdc',MSdef.DC,'Vac',0,'SaveCurrent','no');
    Netlist{end+1} = ADSaddR(-1,'R_MS_ac','p',MSdef.MSnode{1},'n','MSinternalNode','R',MSdef.Rout);
end

end

% @generateFunctionHelp
% @tagline determines the S-matrix of the stages and the package automatically

% @author Adam Cooman
% @institution ELEC VUB

% @output1 contains the S-parameters of each stage in a field that has the
% @output1 name of the stage in the circuit. Besides, it contains a PACKAGE field
% @output1 which holds the S-parameters of the package and it can contain a LOAD
% @output1 field that contains the S-parameters of the seen from the inputs of the total system.
% @outputType1 struct

% @extra Zref is a reserved variable that is added by this function

% @description The ports of the stages and system are indicated with current probes pointing into the stage. 
% @description The current probe should have the following name: I_STAGENAME_py
% @description Where STAGENAME indicates the name of the stage and y indicates the port number.
% @description For the ports of the total system, use "TOTAL" as stagename.
% @description If an input is differential, add 'p' and 'm' to the port number to
% @description indicate the plus and the minus port respectively. 

% @example Here are some examples of port names:
% @example   The current probe at the second port of the INPUT stage should be called I_INPUT_p2. 
% @example   The current probe at first external port should be called I_TOTAL_p1.
% @example   The current probe at the differential port should be called I_AMP_p1p
% @example   and the second port, which is the negative input should be called I_AMP_p1m

%% generateFunctionHelp: old help, backed up at 01-Jun-2015. leave this at the end of the function
% Linear_Analysis_v2 determines the S-matrix of the stages and the package automatically
% 
%      BLAs = Linear_Analysis_v2(NetlistFile);
%      BLAs = Linear_Analysis_v2(NetlistFile,'ParamName',paramValue,...);
% 
% 
% Required inputs:
% - NetlistFile      check: @(x) iscellstr(x)||ischar(x)
%      ADS netlist that holds the circuit. It should also contain several
%       current probes with a name I_XXX_pY where XXX is the name of
%      the stage  under consideration.  Make sure you replace any multisine
%      sources by their output impedance, to  have the correct DC operating
%      point.
% 
% Parameter/Value pairs:
% - 'fstart'     default: 0  check: @isscalar
%      start frequency of the S-parameter simulation
% - 'fstop'     default: []  check: @isscalar
%      stop frequency of the S-parameter simulation
% - 'fstep'     default: []  check: @isscalar
%      frequency step used in the S-parameter simulation
% - 'Z0'     default: 50  check: @isscalar
%      reference impedance of the S-parameters
% - 'unbal2bal'     default: true  check: @islogical
%      boolean that indiactes to use conversion to mixed-mode or not
% - 'Display'     default: 0  check: @islogical
%      to plot, or not to plot?
% - 'onlyInterconnect'     default: false  check: @islogical
%      If only the interconnection has to be calculated, you can set
%      this boolean to true if you only need Sp
% - 'cleanup'     default: true  check: @islogical
%      if this is false, the temporary files are not deleted after
%      the simulation
% - 'onlyTotal'     default: false  check: @islogical
%      if this is true, only the S-parameters of the total system are
%      determined
% - 'getLoadImps'     default: false  check: @islogical
%      indicate whether you want to determine the load impedances as
%      well
% 
% Outputs:
% - BLAs      Type: struct
%      contains the S-parameters of each stage in a field that has
%      the name of the stage in the circuit. Besides, it contains a
%      PACKAGE field which holds the S-parameters of the package and
%      it can contain a LOAD field that contains the S-parameters of
%      the seen from the inputs of the total system.
% 
% Example:
%   
%   The current probe at the second port of the INPUT stage should be called I_INPUT_p2. 
%   The current probe at first external port should be called I_TOTAL_p1.
%   The current probe at the differential port should be called I_AMP_p1p
%   and the second port, which is the negative input should be called I_AMP_p1m
% 
% Zref is a reserved variable that is added by this function NETLIST CONSTRAINTS:
%   The ports of the stages and system are indicated with current probes pointing
% into the stage.    The current probe should have the following name: I_STAGENAME_py
%   Where STAGENAME indicates the name of the stage and y indicates the port number.
%   For the ports of the total system, use "TOTAL" as stagename.   If an input is
% differential, add 'p' and 'm' to the port number to   indicate the plus and the
% minus port respectively. 
% 
% Adam Cooman, ELEC VUB
% 
% 
% This documentation was generated with the generateFunctionHelp function at 01-Jun-2015
