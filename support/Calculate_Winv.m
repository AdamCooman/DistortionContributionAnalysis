function Winv = Calculate_Winv(Sp,Sd)
% for every frequency:
%
% S matrix with everything in it:
% | X X             |   matrix that describes the terminations
% | X X             |
% |     X X X X     |
% |     X X X X     |   package matrix
% |     X X X X     |   
% |     X X X X     |
% |             X X |   stage
% |             X X |
%
% gamma matrix indicating the connections:
% | 0 0 1           |   terminations
% | 0 0   1         |
% | 1   0 0 0 0     |
% |   1 0 0 0 0     |   package matrix
% |     0 0 0 0 1   |   
% |     0 0 0 0   1 |
% |         1   0 0 |   stage
% |           1 0 0 |
%
% W = Gamma - S
% Winv = pinv(W);


% The input is a struct, All variables are saved in there
%   The struct has the following fields: 
%       Stage1 ... StageX with a NxNxF field G that contains the S-parameters
%       PACKAGE field that contains the package S matrix and a cell array thay indicates which port is connedted where
%       LOAD contains the load impedances seen from the total port. this one can be ignored here
%       (possibly) TOTAL a field with the BLA of the total circuit
if isstruct(Sp);
    % look into the portNames of the package to find which stage is connected where
    StageNames = regexp(Sp.PACKAGE.portNames,'_p[0-9]+p?m?c?d?$','split');
    StageNames = unique(cellfun(@(x) x{1},StageNames,'UniformOutput',false),'stable');
    StageCell={};
    for ii=1:length(StageNames);
        % the extrnal ports of the package are denoted with TOTAL, ignore those
        if ~strcmp(StageNames{ii},'TOTAL')
            StageCell{end+1} = Sp.(StageNames{ii}).G;
        end
    end
    Sd = blkdiag_3d(StageCell);
    Sl = Sp.LOAD.G;
    Sp = Sp.PACKAGE.G;
end

[P,~,F]  = size(Sp); % P is the amount of ports in the package
[S,~,F2] = size(Sd); % S is the amount of ports in the stages

if F~=F2
    error('both matrices should be evaluated at the same frequencies');
end
clear F2

% E is the amount of external ports
E = P - S;
% preallocate the matrix
Sbig = zeros([E+P+S E+P+S F]);

% the load matrix first
Sbig(1:E,1:E,:) = Sl;
% The package matrix second
Sbig(E+1:E+P,E+1:E+P,:) = Sp;
% the stages matrix comes last
Sbig(E+P+1:end,E+P+1:end,:) = Sd;

% Build the connection matrix
Gamma = zeros(E+P+S);
% connect the loads to the external terminals of the package
Gamma(E+1:2*E,1:E)=eye(E);
% connect all the stages to the internal terminals
Gamma(2*E+S+1:end,2*E+1:2*E+S)=eye(S);
% make the matrix symmetrical
Gamma = Gamma + Gamma.';

% determine the inverse of the connection scattering matrix
Winv = zeros(size(Sbig));
for ff=1:F
    Winv(:,:,ff) = pinv(Gamma - Sbig(:,:,ff));
end


end