function BLAs = checkBLAquality(BLAs,waves,varargin)



p = inputParser();
p.addRequired('BLAs',@isstruct);
p.addRequired('waves',@isstruct);

p.parse(BLAs,waves,varargin{:});
args = p.Results;
clear p BLAs waves varargin

% get the frequency bins in the waves struct that are inside the band of the BLA
inBandBins = find((args.waves.freq>=min(args.BLAs.freq))&(args.waves.freq<=max(args.BLAs.freq)));
F = length(inBandBins);

fields = fieldnames(args.BLAs);
fields = fields(~cellfun(@(x) any(strcmpi(x,{'freq','freq_lin','PACKAGE','LOAD','TERMINATED','TOTAL'})),fields,'UniformOutput',true));
% TODO, make a filter to filter out the unwanted stages in the args.stagesToCheck field

% get the amount of realisations
M = size(args.waves.(sprintf('W_%s_p%s_B',fields{1},args.BLAs.(fields{1}).porttype{1})),1);

for ss = 1:length(fields)
    % P is the amount of ports in the sub-circuit
    P = size(args.BLAs.(fields{ss}).G,1);
    % B and A will contain the B and A waves around the circuit for the
    % different realisations of the multisine
    B = zeros(P,M,F);
    A = zeros(P,M,F);
    % gather all the waves
    for mm=1:M
        for pp=1:P
            B(pp,mm,:) = args.waves.(sprintf('W_%s_p%s_B',fields{ss},args.BLAs.(fields{ss}).porttype{pp}))(mm,1,inBandBins);
            A(pp,mm,:) = args.waves.(sprintf('W_%s_p%s_A',fields{ss},args.BLAs.(fields{ss}).porttype{pp}))(mm,1,inBandBins);
        end
    end
    
    % D will contain the power of the distortion sources over the different frequencies
    args.BLAs.(fields{ss}).D = zeros(P,F);
    for ff=1:F
        % calculate B - S*A for all the realisations
        temp = zeros(P,M);
        for mm=1:M
            temp(:,mm) = B(:,mm,ff) - args.BLAs.(fields{ss}).G(:,:,ff)*A(:,mm,ff);
        end
        % calculate the rms of the distortion over the different realisations
        args.BLAs.(fields{ss}).D(:,ff) = sqrt(mean(abs(temp).^2,2));
    end
    
    % if the G_linear field is present, perform the same thing with G_linear
    if isfield(args.BLAs.(fields{ss}),'G_linear');
        args.BLAs.(fields{ss}).D_linear = zeros(P,F);
        for ff=1:F
            % calculate B - S*A for all the realisations
            temp = zeros(P,M);
            for mm=1:M
                temp(:,mm) = B(:,mm,ff) - args.BLAs.(fields{ss}).G_linear(:,:,ff)*A(:,mm,ff);
            end
            % calculate the rms of the distortion over the different realisations
            args.BLAs.(fields{ss}).D_linear(:,ff) = sqrt(mean(abs(temp).^2,2));
        end
    end
end

BLAs = args.BLAs;



end



