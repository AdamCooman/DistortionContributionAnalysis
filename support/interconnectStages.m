function Spd = interconnectStages(Sp,Sd,Stages)
% this function interconnects the stages in the Sd matrix with the
% interconnection network in the Sp matrix to obtain the S-parameters of
% the total system
%
%   Spd = interconnectStages(Sp,Sd,Stages)
%
% where
%   Sp is the [sum(Stages)+Extern x sum(Stages)+Extern x F] matrix that contains the S-parameters of the package
%   Sd is the block diagonal [sum(Stages) x sum(Stages) x F] matrix that contains the S-parameters of the stages
%   Stages is a vector which contains the amount of ports per stage.
%
% Adam Cooman, ELEC VUB

if isstruct(Sp)
    % Sp is a struct. It contains the fields PACKAGE, freq and a whole
    % collection of different stages. each of the fields has the BLA in its
    % G field.
    BLAs = Sp;
    clear Sp
    
    % check the portnames field of the PACKAGE for the connections
    packageports = regexp(BLAs.PACKAGE.portNames,'_p\d+d?c?p?m?','split');
    [packageports,~,ia] = unique(cellfun(@(x) x{1},packageports,'uniformoutput',false),'stable');
    
    
    Extern = sum(ia==1);
    Intern = 0;
    Smats{1} = BLAs.PACKAGE.G;
    conn=[];
    for ii=2:length(packageports)
        % put the S-parameters in a cell array
        Smats{end+1} = BLAs.(packageports{ii}).G;
        P = size(Smats{end},1);
        % build the connection matrix
        for pp=1:P
            conn = [conn;1 Extern+Intern+pp ii pp];
        end
        Intern = Intern+P;
    end
    
    % interconnect all the stages
    BLAs.TOTAL.G_conn = multiportConnection(Smats,conn);
    
    Spd=BLAs;
    
else
    Extern = size(Sp,1)-size(Sd,1);
    
    % connect all the stages into the package to obtain the S-matrix of the total amplifier
    conn=[];
    for ss=1:length(Stages)
        for pp=1:Stages(ss)
            conn=[conn;1 Extern+sum(Stages(1:ss-1))+pp ss+1 pp];%#ok
        end
    end
    % put the S-matrices of the stages in a cell array to allow using the multiportConnection function to connect everything together
    Sdcell = cell(length(Stages),1);
    for ss=1:length(Stages)
        range = sum(Stages(1:ss-1))+1:sum(Stages(1:ss));
        Sdcell{ss}=Sd(range,range,:);
    end
    % do the actual connection
    Spd = multiportConnection([{Sp};Sdcell],conn);
    
end

end