function [BLAs,spec,waves] = Substract_Analysis(Netlist,MSdef,varargin)


p = inputParser();
% ADS netlist that contains the circuit
p.addRequired('Netlist',@(x) iscellstr(x)||ischar(x));
% definition of the large multisine in the circuit
p.addRequired('MSdef',@isstruct);
% number of realisations used in the estimation of the BLA
p.addParameter('numberOfRealisations',3,@isnatural);
% maximum crest factor of the multisines
p.addParameter('CF',Inf,@isscalar);
% simulation technique used in the large-signal simulation
p.addParameter('simulator','HB',@(x) any(strcmpi(x,{'HB','TRAN','ENV'})));
% estimated order of non-linearity of the circuit
p.addParameter('oversample',10,@isnatural);
% indicates which stages of the circuit should be estimated. If this is
% left empty, all stages are estimated
p.addParameter('StagesToEstimate',{},@iscellstr);
% if this is true, the large multisine will be used as one of the
% excitation signals. Otherwise only ticklers are used in the
% identification
p.addParameter('useLargeMS',true,@islogical);
% This parameter controls the conversion from unbalanced to balanced
% representation for differential inputs
p.addParameter('unbal2bal',true,@islogical);
% if this is set to true, FOMS ticklers are used, otherwise the tickler
% sources are activated one by one
p.addParameter('FOMStickler',false,@islogical);
% if this is set to true, the FRM is also estimated using an AC analysis
p.addParameter('ACcheck',false,@islogical);
p.parse(Netlist,MSdef,varargin{:});
args = p.Results();
clear Netlist MSdef varargin p

% if needed, read the provided file into a cell array of strings
if ischar(args.Netlist) 
    args.Netlist = readTextFile(args.Netlist);
end 

% ensure that MSnode is a cell array
if ~iscell(args.MSdef.MSnode)
    args.MSdef.MSnode = {args.MSdef.MSnode '0'};
end

% make sure the voltage at each current source is measured
args.Netlist = prepareWaveSimulation(args.Netlist);

% analyse the netlist to obtain the amount of ports for each stage in the netlist
[info.Extern,info.Stages,info.StageNames,...
    info.externNodes,info.StageNodes,...
    info.diffExPorts,info.diffStPorts] = analyseNetlist(args.Netlist);

% if the user provides the stages that have to be estimated, then remove
% all the others from the info list
info = selectStages(info,args.StagesToEstimate);
if ~isempty(args.StagesToEstimate)
    estimateTOTAL = any(strcmpi(args.StagesToEstimate,'TOTAL'));
else
    estimateTOTAL = true;
end

% sort the stages by amount of ports
info = sortStages(info);

% collect the inputs and outputs names in a cell array
names = {};
inputs = {};
outputs = {};
if estimateTOTAL
    names{end+1} = 'TOTAL';
    inputs{end+1}={};
    outputs{end+1}={};
    for pp=1:(info.Extern-length(info.diffExPorts))
        if ~any(info.diffExPorts==pp)
            inputs{1}{end+1}  = sprintf('W_TOTAL_p%d_A',pp);
            outputs{1}{end+1} = sprintf('W_TOTAL_p%d_B',pp);
        else
            if ~args.unbal2bal
                inputs{1}{end+1}  = sprintf('W_TOTAL_p%dp_A',pp);
                inputs{1}{end+1}  = sprintf('W_TOTAL_p%dm_A',pp);
                outputs{1}{end+1} = sprintf('W_TOTAL_p%dp_B',pp);
                outputs{1}{end+1} = sprintf('W_TOTAL_p%dm_B',pp);
            else
                inputs{1}{end+1}  = sprintf('W_TOTAL_p%dd_A',pp);
                inputs{1}{end+1}  = sprintf('W_TOTAL_p%dc_A',pp);
                outputs{1}{end+1} = sprintf('W_TOTAL_p%dd_B',pp);
                outputs{1}{end+1} = sprintf('W_TOTAL_p%dc_B',pp);
            end
        end
    end
end

for ss=1:length(info.Stages)
    names{end+1}=info.StageNames{ss};%#ok
    inputs{end+1} = {};%#ok
    outputs{end+1}= {};%#ok
    for pp=1:info.Stages(ss)-length(info.diffStPorts{ss})
        if ~any(info.diffStPorts{ss}==pp)
            inputs{end}{end+1}  = sprintf('W_%s_p%d_A',info.StageNames{ss},pp);
            outputs{end}{end+1} = sprintf('W_%s_p%d_B',info.StageNames{ss},pp);
        else
            if ~args.unbal2bal
                inputs{end}{end+1}  = sprintf('W_%s_p%dp_A',info.StageNames{ss},pp);
                inputs{end}{end+1}  = sprintf('W_%s_p%dm_A',info.StageNames{ss},pp);
                outputs{end}{end+1} = sprintf('W_%s_p%dp_B',info.StageNames{ss},pp);
                outputs{end}{end+1} = sprintf('W_%s_p%dm_B',info.StageNames{ss},pp);
            else
                inputs{end}{end+1}  = sprintf('W_%s_p%dd_A',info.StageNames{ss},pp);
                inputs{end}{end+1}  = sprintf('W_%s_p%dc_A',info.StageNames{ss},pp);
                outputs{end}{end+1} = sprintf('W_%s_p%dd_B',info.StageNames{ss},pp);
                outputs{end}{end+1} = sprintf('W_%s_p%dc_B',info.StageNames{ss},pp);
            end
        end
    end
end

% build the list of all the nodes that should be excited. I put the nodes
% of the highest port numbers first, because they should be furthest away
% from the large source. The nodes of the total circuit are also put first,
% because that has to be estimated with ticklers outside its network.
nodeList = {};
if estimateTOTAL
    for pp=info.Extern:-1:1
        nodeList{end+1}=info.externNodes{pp}{1};
    end
end
for ss=1:length(info.Stages)
    for pp=info.Stages(ss):-1:1
        nodeList{end+1}=info.StageNodes{ss}{pp}{1};
    end
end

% if the output impedance of the multisine source is zero, we cannot
% connect a current source to that node. So if it's in the nodelist, remove
% that node from the nodelist
if args.MSdef.Rout==0
    nodeList = nodeList(~strcmp(args.MSdef.MSnode{1},nodeList));
end

% the amount of ticklers needed is equal to the maximum amount of ports of
% one of the sub-circuits minus one, because the multisine does stuff as well
if args.useLargeMS
    T = max([info.Extern info.Stages])-1;
else
    T = max([info.Extern info.Stages]);
end
ticklernodes = nodeList(1:T);

% the choice of extra nodes can probably be improved. Now, just the first
% nodes are chosen, but it is not guaranteed that this is the best option

%% Run the simulations
[spec,AC] = runSubstractSimulation(args.Netlist,args.MSdef,...
    'TicklerNodes',ticklernodes,'measure',true,...
    'numberOfRealisations',args.numberOfRealisations,...
    'simulator',args.simulator,'oversample',args.oversample,...
    'FOMStickle',args.FOMStickler,'ACcheck',args.ACcheck);

% calculate the waves in the simulation
waves = makeallwaves(spec,50,'pseudo',true);

% the reference signal in the BLA estimation is given by the tickler currents
reference={};
if args.useLargeMS
    reference{end+1} = args.MSdef.MSnode{1};
end
for pp=1:length(ticklernodes)
    reference{end+1} = sprintf('I_tickle%d',pp); %#ok
end

% also add the reference currents to the waves struct
for pp=1:length(reference)
    waves.(reference{pp}) = spec.(reference{pp});
end

% to make sure that the main multisine is really gone in the circuit when the ticklers are present, set it to zero
if args.useLargeMS
    for mm=1:args.numberOfRealisations
        waves.(reference{1})((mm-1)*(T+1)+1+(1:T),1,:) = 0;
    end
end

%% now identify all the BLAs in the circuit
% S is the number of subcircuits
S = length(names);

if args.useLargeMS
    off = 0;
else
    off = 1;
end

% Reshaping the reference signals into a nice reference matrix
nr = length(reference);
M = args.numberOfRealisations;
F = length(args.MSdef.grid);
R = zeros(nr,nr,M,F);
for mm=1:M
    for rr=1:nr
        R(rr,:,mm,:) = waves.(reference{rr})((mm-1)*(nr+off)+(1:nr)+off,1,args.MSdef.grid+1);
    end
end
   
% if the tickler is not a FOMS, then we have a diagonal R matrix, set the
% extra elements to zero then, to avoid weird effects
if args.FOMStickler
    for rr=1:nr
        for rrr=1:nr
            if rr~=rrr
                R(rr,rrr,:,:) = 0;
            end
        end
    end
end

% now estimate the different stages
for ss=1:S
    % for the TOTAL, we cannot use internal signals
    if strcmpi(names{ss},'TOTAL')
        % gather the input and output signals
        P = info.Extern;
        U = zeros(P,P,M,F);
        Y = zeros(P,P,M,F);
        for mm=1:M
            for pp=1:P
                U(pp,:,mm,:) = waves.( inputs{1}{pp})((mm-1)*(nr+off)+(1:P)+off,1,args.MSdef.grid+1);
                Y(pp,:,mm,:) = waves.(outputs{1}{pp})((mm-1)*(nr+off)+(1:P)+off,1,args.MSdef.grid+1);
            end
        end
        % estimate the BLA
        [BLAs.TOTAL.G,BLAs.TOTAL.CvecG] = extractMIMO_BLA_v2(U,Y,'R',R(1:P,1:P,:,:));
    else
        P = length(inputs{ss});
        % fill the Y and U matrices
        U = zeros(P,nr,M,F);
        Y = zeros(P,nr,M,F);
        for mm=1:M
            for pp=1:P
                U(pp,:,mm,:) = waves.( inputs{ss}{pp})((mm-1)*(nr+off)+(1:nr)+off,1,args.MSdef.grid+1);
                Y(pp,:,mm,:) = waves.(outputs{ss}{pp})((mm-1)*(nr+off)+(1:nr)+off,1,args.MSdef.grid+1);
            end
        end
        % estimate the BLA
        [BLAs.(names{ss}).G,~,~,~,~,BLAs.(names{ss}).CvecG] = extractMIMO_BLA_v2(U,Y,'R',R,'plot',true);
    end
    
    % ignore the covariance and just save the variance in a matrix that has the
    % same size as the BLA itself
    BLAs.(names{ss}).varG = zeros(size(BLAs.(names{ss}).G));
    for ff = 1:F
        BLAs.(names{ss}).varG(:,:,ff) = reshape(diag(squeeze(BLAs.(names{ss}).CvecG(:,:,ff))) , [ P , P ] );
    end
end

% if wanted, calculate the FRM using the AC simulations
if args.ACcheck
    % convert the voltages and currents into waves
    ACwaves = makeallwaves(AC,50,'pseudo',true);
    
    % if the large multisine is not used, remove it from the AC simulations
    if ~args.useLargeMS
        ACsigs = fieldnames(ACwaves);
        for ss=1:length(ACsigs)
            if ~strcmp(ACsigs{ss},'freq');
                ACwaves.(ACsigs{ss}) = ACwaves.(ACsigs{ss})(2:end,:);
            end
        end
    end
    
    % get the length of the frequency axis
    F_AC = length(AC.freq);
    
    % estimate the AC FRF of the different stages
    for ss=1:S
        % for the TOTAL, we cannot use internal signals
        if strcmpi(names{ss},'TOTAL')
            % gather the input and output signals
            P = info.Extern;
            U = zeros(P,P,1,F_AC);
            Y = zeros(P,P,1,F_AC);
            for pp=1:P
                U(pp,:,1,:) = ACwaves.( inputs{1}{pp});
                Y(pp,:,1,:) = ACwaves.(outputs{1}{pp});
            end
            % estimate the BLA
            BLAs.TOTAL.G_AC = extractMIMO_BLA_v2(U,Y);
        else
            P = length(inputs{ss});
            % fill the Y and U matrices
            U = zeros(P,nr,1,F_AC);
            Y = zeros(P,nr,1,F_AC);
            for pp=1:P
                U(pp,:,1,:) = ACwaves.( inputs{ss}{pp});
                Y(pp,:,1,:) = ACwaves.(outputs{ss}{pp});
            end
            % estimate the BLA
            BLAs.(names{ss}).G_AC = extractMIMO_BLA_v2(U,Y);
        end
    end
end

% convert to a balanced representation if needed
if args.unbal2bal
    % TODO 
    
end

% add the frequency axis
BLAs.freq = waves.freq(args.MSdef.grid+1);

% remove the reference signal from the waves struct
for pp=1:length(reference)
    waves = rmfield(waves,reference{pp});
end

% disp('the tickled nodes were:');
% disp(ticklernodes);

end

%% function to sort stages according to their amount of ports
function info = sortStages(info)
% sorts the stages on the amount of ports
[info.Stages,order] = sort(info.Stages,'descend');
info.StageNames = info.StageNames(order);
info.StageNodes = info.StageNodes(order);
info.diffStPorts= info.diffStPorts(order);
end

%% function to remove unwanted bins from the BLA results
function BLA = cleanBLAgrid(BLA,MSdef)
% this function interpolates and extrapolates the BLA to the grid of the MSdef
freqnew = MSdef.f0*(min(MSdef.grid):max(MSdef.grid));
% now just interpolate all the things. There will be some extrapolation at
% the edge, but if the ticlers lie close enough to the actual multisine
% grid, I don't care about that
BLA.G = interpolate_MIMO(BLA.freq,BLA.G,freqnew);
BLA.CvecG = interpolate_MIMO(BLA.freq,BLA.CvecG,freqnew);
BLA.varG = interpolate_MIMO(BLA.freq,BLA.varG,freqnew);
BLA.freq = freqnew;
end

function info = selectStages(info,StagesToEstimate)
% this function allows to select some of the stages only, to avoid having
% to estimate them all. This is done by removing several of the stages from
% the info struct. That way, it appears the other stages don't exist
if ~isempty(StagesToEstimate)
    % check whether the specified stages are actual stages in the circuit.
    % and save their index in the stageList
    inds = [];
    for ss=1:length(StagesToEstimate)
        if ~strcmpi(StagesToEstimate{ss},'TOTAL')
            ind = find(strcmp(StagesToEstimate{ss},info.StageNames));
            if isempty(ind)
                error(['Stage "' StagesToEstimate{ss} '" has not been found in the circuit']);
            else
                inds(end+1) = ind;
            end
        end
    end
    % now remove all the other stages from the lists
    info.Stages = info.Stages(inds);
    info.StageNames = info.StageNames(inds);
    info.StageNodes = info.StageNodes(inds);
    info.diffStPorts = info.diffStPorts(inds);
end
end

% @generateFunctionHelp

% @author Adam Cooman
% @institution ELEC, VUB